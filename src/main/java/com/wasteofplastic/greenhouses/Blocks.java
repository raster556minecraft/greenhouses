package com.wasteofplastic.greenhouses;

import org.bukkit.Material;

import java.util.ArrayList;
import java.util.List;

public class Blocks {
    // Valid Roof Blocks
    public static final List<Material> roofBlocks = new ArrayList<>();
    // Valid Wall Blocks
    public static final List<Material> wallBlocks = new ArrayList<>();
    // Roof Blocks to look for normaly
    public static final List<Material> roofDetectBlocks = new ArrayList<>();
    // Wall Blocks to look for normaly
    public static final List<Material> wallDetectBlocks = new ArrayList<>();
}
