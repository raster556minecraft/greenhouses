package com.wasteofplastic.greenhouses;

import com.wasteofplastic.greenhouses.greenhouse.Greenhouse;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Tracks the following info on the player
 * UUID, player name, whether they are in a greenhouse or not and how many greenhouses they have
 */
class Players {
    private final UUID uuid;
    private Greenhouse inGreenhouse;
    private int numberOfGreenhouses;

    /**
     * @param player
     *            Constructor - initializes the state variables
     *
     */
    public Players(final Player player) {
        this.uuid = player.getUniqueId();
        // We do not know if the player is in a greenhouse or not yet
        this.inGreenhouse = null;
        // We start with the assumption they have no greenhouses yet
        this.numberOfGreenhouses = 0;
    }

    /**
     * @return A greenhouse if a player is in the greenhouse
     */
    public Greenhouse getInGreenhouse() {
        return inGreenhouse;
    }

    /**
     * @param inGreenhouse The greenhouse the user is in
     */
    public void setInGreenhouse(Greenhouse inGreenhouse) {
        this.inGreenhouse = inGreenhouse;
    }

    /**
     * @return The number of greenhouses for the player
     */
    public int getNumberOfGreenhouses() {
        return numberOfGreenhouses;
    }

    public void incrementGreenhouses() {
        numberOfGreenhouses++;
    }

    public void decrementGreenhouses() {
        numberOfGreenhouses--;
        if (numberOfGreenhouses < 0) {
            numberOfGreenhouses = 0;
        }
    }

    /**
     * @return The player UUID
     */
    public UUID getUuid() {
        return uuid;
    }

}
