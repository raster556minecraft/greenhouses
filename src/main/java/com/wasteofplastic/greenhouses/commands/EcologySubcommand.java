package com.wasteofplastic.greenhouses.commands;

import com.wasteofplastic.greenhouses.Greenhouses;
import com.wasteofplastic.greenhouses.greenhouse.BiomeRecipe;
import com.wasteofplastic.greenhouses.ui.Locale;
import com.wasteofplastic.greenhouses.util.Util;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class EcologySubcommand extends Subcommand {


    public EcologySubcommand(Greenhouses plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return Locale.helpecology;
    }

    @Override
    public String getUsage(String lable) {
        return "/" + lable + " ecology <number>";
    }

    @Override
    public String getPermission() {
        return "greenhouses.player";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        final Player player = (Player) sender;

        if (args.length < 2) {
            player.sendMessage(ChatColor.RED + Locale.recipehint);
            return true;
        }

        player.sendMessage(" ");
        int recipeNumber;
        try {
            recipeNumber = Integer.valueOf(args[1]);
        } catch (Exception e) {
            player.sendMessage(ChatColor.RED + Locale.recipehint);
            return true;
        }
        List<BiomeRecipe> recipeList = plugin.getBiomeRecipes();
        if (recipeNumber <1 || recipeNumber > recipeList.size()) {
            player.sendMessage(ChatColor.RED + Locale.recipewrongnumber.replace("[size]", String.valueOf(recipeList.size())));
            return true;
        }
        BiomeRecipe br = recipeList.get(recipeNumber-1);
        if (br.getFriendlyName().isEmpty()) {
            player.sendMessage(ChatColor.GREEN + "[" + Util.prettifyText(br.getBiome().toString()) + " ecology" + ChatColor.GREEN + "]");
        } else {
            player.sendMessage(ChatColor.GREEN + "[" + br.getFriendlyName() + " ecology" + ChatColor.GREEN + "]");
        }
        player.sendMessage(ChatColor.YELLOW + "Biome: " + Util.prettifyText(br.getBiome().toString()));

        // Plants
        // Plant Material, Material on which to grow, Probability
        List<Material> plantMaterial = br.getPlants();
        List<Integer> plantProbability = br.getPlantProbabilities();
        List<Material> plantGrownOn = br.getPlantRequiredBlocks();
        // Mobs
        // Entity Type, Material to Spawn on, Probability
        List<EntityType> mobType = br.getMobSpawns();
        List<Double> mobProbability = br.getSpawnChance();
        List<Material> mobSpawnOn = br.getBlockSpawnOn();
        // Conversions
        // Original Material, New Material, Probability
        List<Material> oldMaterial = br.getConvertOld();
        List<Integer> convChance = br.getConvertChance();
        List<Material> newMaterial = br.getConvertNew();
        List<Material> localMaterial = br.getConvertRequired();
        if (!plantMaterial.isEmpty()) {
            player.sendMessage("[Plants]");
            for (int i = 0; i < plantMaterial.size(); i++) {
                player.sendMessage(ChatColor.YELLOW +  Util.getName(new ItemStack(plantMaterial.get(i), 1)) + ", " + plantProbability.get(i) + "% when on " + Util.getName(new ItemStack(plantGrownOn.get(i))));
            }
        }
        if (!mobType.isEmpty()) {
            player.sendMessage("[Mobs]");
            for (int i = 0; i < mobType.size(); i++) {
                player.sendMessage(ChatColor.YELLOW + Util.prettifyText(mobType.get(i).toString()) + ", " + (int)(mobProbability.get(i)*100) + "% when on " + Util.getName(new ItemStack(mobSpawnOn.get(i), 1)));
            }
        }
        if (!oldMaterial.isEmpty()) {
            player.sendMessage("[Conversions]");
            for (int i = 0; i < oldMaterial.size(); i++) {
                if (localMaterial.get(i) == null) {
                    player.sendMessage(ChatColor.YELLOW + convChance.get(i).toString() + "% of " + Util.getName(new ItemStack(oldMaterial.get(i), 1)) + " --> " +
                            Util.getName(new ItemStack(newMaterial.get(i), 1)));
                } else {
                    player.sendMessage(ChatColor.YELLOW + convChance.get(i).toString() + "% of " + Util.getName(new ItemStack(oldMaterial.get(i), 1)) + " --> " +
                            Util.getName(new ItemStack(newMaterial.get(i), 1)) + "; when next to " + Util.getName(new ItemStack(localMaterial.get(i))));
                }
            }
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String lable, String[] args) {
        if (!(sender instanceof Player))
            return null;
        Player player = (Player) sender;

        if (args.length != 2)
            return null;

        List<String> tabList = new ArrayList<>();
        for (int i = 1; i <= plugin.getBiomeRecipes().size(); i++) {
            tabList.add(String.valueOf(i));
        }
        return tabList;
    }
}