package com.wasteofplastic.greenhouses.commands;

import com.wasteofplastic.greenhouses.Greenhouses;
import com.wasteofplastic.greenhouses.greenhouse.BiomeRecipe;
import com.wasteofplastic.greenhouses.ui.Locale;
import com.wasteofplastic.greenhouses.util.Util;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class RecipeSubcommand extends Subcommand {


    public RecipeSubcommand(Greenhouses plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return Locale.helprecipe;
    }

    @Override
    public String getUsage(String lable) {
        return "/" + lable + " recipe <number>";
    }

    @Override
    public String getPermission() {
        return "greenhouses.player";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        final Player player = (Player) sender;

        if (args.length < 2) {
            player.sendMessage(ChatColor.RED + Locale.recipehint);
            return true;
        }

        player.sendMessage(" ");
        int recipeNumber;
        try {
            recipeNumber = Integer.valueOf(args[1]);
        } catch (Exception e) {
            player.sendMessage(ChatColor.RED + Locale.recipehint);
            return true;
        }
        List<BiomeRecipe> recipeList = plugin.getBiomeRecipes();
        if (recipeNumber <1 || recipeNumber > recipeList.size()) {
            player.sendMessage(ChatColor.RED + Locale.recipewrongnumber.replace("[size]", String.valueOf(recipeList.size())));
            return true;
        }
        BiomeRecipe br = recipeList.get(recipeNumber-1);
        if (br.getFriendlyName().isEmpty()) {
            player.sendMessage(ChatColor.GREEN + "[" + Util.prettifyText(br.getBiome().toString()) + " recipe" + ChatColor.GREEN + "]");
        } else {
            player.sendMessage(ChatColor.GREEN + "[" + br.getFriendlyName() + " recipe" + ChatColor.GREEN + "]");
        }
        player.sendMessage(ChatColor.YELLOW + "Biome: " + Util.prettifyText(br.getBiome().toString()));
        if (br.getWaterCoverage() == 0) {
            player.sendMessage(Locale.recipenowater);
        } else if (br.getWaterCoverage() > 0) {
            player.sendMessage(Locale.recipewatermustbe.replace("[coverage]", String.valueOf(br.getWaterCoverage())));
        }
        if (br.getIceCoverage() == 0) {
            player.sendMessage(Locale.recipenoice);
        } else if (br.getIceCoverage() > 0) {
            player.sendMessage(Locale.recipeicemustbe.replace("[coverage]", String.valueOf(br.getIceCoverage())));
        }
        if (br.getLavaCoverage() == 0) {
            player.sendMessage(Locale.recipenolava);
        } else if (br.getLavaCoverage() > 0) {
            player.sendMessage(Locale.recipelavamustbe.replace("[coverage]", String.valueOf(br.getLavaCoverage())));
        }
        List<String> reqBlocks = br.getRecipeBlocks();
        if (reqBlocks.size() > 0) {
            player.sendMessage(ChatColor.YELLOW + Locale.recipeminimumblockstitle);
            int index = 1;
            for (String list : reqBlocks) {
                player.sendMessage(Locale.lineColor + (index++) + ": " + list);
            }
        } else {
            player.sendMessage(ChatColor.YELLOW + Locale.recipenootherblocks);
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String lable, String[] args) {
        if (!(sender instanceof Player))
            return null;
        Player player = (Player) sender;

        if (args.length != 2)
            return null;

        List<String> tabList = new ArrayList<>();
        for (int i = 1; i <= plugin.getBiomeRecipes().size(); i++) {
            tabList.add(String.valueOf(i));
        }
        return tabList;
    }
}