package com.wasteofplastic.greenhouses.commands;

import com.wasteofplastic.greenhouses.Greenhouses;
import com.wasteofplastic.greenhouses.greenhouse.BiomeRecipe;
import com.wasteofplastic.greenhouses.ui.Locale;
import com.wasteofplastic.greenhouses.util.Util;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class ListSubcommand extends Subcommand {


    public ListSubcommand(Greenhouses plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return Locale.helplist;
    }

    @Override
    public String getUsage(String lable) {
        return "/" + lable + " list";
    }

    @Override
    public String getPermission() {
        return "greenhouses.player";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        final Player player = (Player) sender;

        player.sendMessage(" ");
        // List all the biomes that can be made
        player.sendMessage(ChatColor.GREEN + Locale.listtitle);
        player.sendMessage(Locale.listinfo);
        int index = 0;
        for (BiomeRecipe br : plugin.getBiomeRecipes()) {
            if (br.getFriendlyName().isEmpty()) {
                player.sendMessage(ChatColor.YELLOW + Integer.toString(index++ +1) + ": " + Util.prettifyText(br.getBiome().toString()));
            } else {
                player.sendMessage(ChatColor.YELLOW + Integer.toString(index++ +1) + ": " + br.getFriendlyName());
            }
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String lable, String[] args) {
        return null;
    }
}