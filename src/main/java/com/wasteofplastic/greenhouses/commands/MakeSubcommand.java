package com.wasteofplastic.greenhouses.commands;

import com.wasteofplastic.greenhouses.Greenhouses;
import com.wasteofplastic.greenhouses.PlayerCache;
import com.wasteofplastic.greenhouses.greenhouse.BiomeRecipe;
import com.wasteofplastic.greenhouses.greenhouse.Greenhouse;
import com.wasteofplastic.greenhouses.ui.Locale;
import org.apache.commons.lang.math.NumberUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class MakeSubcommand extends Subcommand {


    public MakeSubcommand(Greenhouses plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return Locale.helpmake;
    }

    @Override
    public String getUsage(String lable) {
        return "/" + lable + " make [<number>]";
    }

    @Override
    public String getPermission() {
        return "greenhouses.player";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        PlayerCache players = plugin.players;
        Player player = (Player) sender;

        if (args.length == 1) {

            // Sets up a greenhouse by finding best fit for biome
            final Greenhouse greenhouseN = players.getInGreenhouse(player);
            if (greenhouseN != null) {
                // alreadyexists
                player.sendMessage(ChatColor.RED + Locale.erroralreadyexists);
                return true;
            }
            // Check if they are at their limit
            if (plugin.players.isAtLimit(player)) {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', Locale.infonomore));
            } else {
                // Try to make greenhouse
                Greenhouse g = plugin.tryToMakeGreenhouse(player, false);
                if (g == null) {
                    // norecipe
                    player.sendMessage(ChatColor.RED + Locale.errornorecipe);
                    return true;
                }
                // Greenhouse is made
            }
        } else if (args.length == 2) {
            // Sets up a greenhouse for a specific biome
            if (players.getInGreenhouse(player) != null) {
                // alreadyexists
                player.sendMessage(ChatColor.RED + Locale.erroralreadyexists);
                return true;
            }
            // Check if they are at their limit
            if (plugin.players.isAtLimit(player)) {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', Locale.infonomore));
            } else {
                // Check we are in a greenhouse
                try {
                    if (NumberUtils.isNumber(args[1])) {
                        int recipeNum = Integer.valueOf(args[1]) -1; // get value and shift it down by one to start at 0
                        List<BiomeRecipe> recipeList = plugin.getBiomeRecipes();
                        if (recipeNum < 0 || recipeNum > recipeList.size()) {
                            player.sendMessage(ChatColor.RED + Locale.errornorecipe);
                            return true;
                        }
                        if (plugin.tryToMakeGreenhouse(player, recipeList.get(recipeNum), false) == null) {
                            // Failed for some reason - maybe permissions
                            player.sendMessage(ChatColor.RED + Locale.errornorecipe);
                            return true;
                        }
                    }
                } catch (Exception e) {
                    player.sendMessage(ChatColor.RED + Locale.errornorecipe);
                    return true;
                }
            }
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String lable, String[] args) {
        if (!(sender instanceof Player))
            return null;
        Player player = (Player) sender;

        if (args.length != 2)
            return null;

        List<BiomeRecipe> br = plugin.getBiomeRecipes();
        List<String> tabList = new ArrayList<>();
        for (int i = 0; i < br.size(); i++) {
            if (player.hasPermission(br.get(i).getPermission()))
                tabList.add(String.valueOf(i+1));
        }
        return tabList;
    }
}
