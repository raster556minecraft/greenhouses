package com.wasteofplastic.greenhouses.commands;

import com.wasteofplastic.greenhouses.Greenhouses;
import com.wasteofplastic.greenhouses.PlayerCache;
import com.wasteofplastic.greenhouses.greenhouse.Greenhouse;
import com.wasteofplastic.greenhouses.ui.Locale;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.List;
import java.util.UUID;

public class GuiSubcommand extends Subcommand {


    public GuiSubcommand(Greenhouses plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return Locale.helpopengui;
    }

    @Override
    public String getUsage(String lable) {
        return "/" + lable + " gui";
    }

    @Override
    public String getPermission() {
        return "greenhouses.player";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        final PlayerCache players = plugin.players;
        final Player player = (Player) sender;
        final UUID playerUUID = player.getUniqueId();

        final Greenhouse greenhouseInNow = players.getInGreenhouse(player);
        if (greenhouseInNow==null || greenhouseInNow.getOwner().equals(playerUUID)) {
            Inventory playerInv = plugin.getMenuInv(player);
            plugin.uiCache.storeMenuUI(player.getUniqueId(), playerInv);
            player.openInventory(playerInv);
            return true;
        } else {
            player.sendMessage(ChatColor.RED + Locale.errornotowner);
            return true;
        }
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String lable, String[] args) {
        return null;
    }
}
