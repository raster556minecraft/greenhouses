package com.wasteofplastic.greenhouses.commands;

import com.wasteofplastic.greenhouses.Greenhouses;
import com.wasteofplastic.greenhouses.PlayerCache;
import com.wasteofplastic.greenhouses.greenhouse.Greenhouse;
import com.wasteofplastic.greenhouses.ui.Locale;
import com.wasteofplastic.greenhouses.util.Util;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class InfoSubcommand extends Subcommand {


    public InfoSubcommand(Greenhouses plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return Locale.helpinfo;
    }

    @Override
    public String getUsage(String lable) {
        return "/" + lable + " info";
    }

    @Override
    public String getPermission() {
        return "greenhouses.player";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        final PlayerCache players = plugin.players;
        final Player player = (Player) sender;

        player.sendMessage(" ");
        // Show some instructions on how to make greenhouses
        player.sendMessage(ChatColor.GREEN + ChatColor.translateAlternateColorCodes('&', Locale.infotitle));
        for (String instructions : Locale.infoinstructions) {
            player.sendMessage(ChatColor.GREEN + ChatColor.translateAlternateColorCodes('&', instructions));
        }
        final Greenhouse greenhouseIn = players.getInGreenhouse(player);
        if (greenhouseIn != null) {
            player.sendMessage(ChatColor.GOLD + Locale.infoinfo);
            // general.biome
            player.sendMessage(ChatColor.GREEN + Locale.generalbiome + ": " + Util.prettifyText(greenhouseIn.getBiome().toString()));
            if (greenhouseIn.getOwner() != null) {
                Player owner = plugin.getServer().getPlayer(greenhouseIn.getOwner());
                if (owner != null) {
                    player.sendMessage(ChatColor.YELLOW + Locale.generalowner + ": " + owner.getDisplayName() + " (" + owner.getName() + ")");
                } else {
                    player.sendMessage(ChatColor.YELLOW + Locale.generalowner + ": " + greenhouseIn.getPlayerName());
                }
            }
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String lable, String[] args) {
        return null;
    }
}