package com.wasteofplastic.greenhouses.commands;

import com.wasteofplastic.greenhouses.Greenhouses;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.util.List;

public abstract class Subcommand {

    Greenhouses plugin;

    public Subcommand (Greenhouses plugin) {
        this.plugin = plugin;
    }

    public abstract String getHelp();
    public abstract String getUsage(String label);
    public abstract String getPermission();
    public abstract boolean onCommand(CommandSender sender, Command cmd, String label, String[] args);
    public abstract List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args);
}
