package com.wasteofplastic.greenhouses.commands;

import com.wasteofplastic.greenhouses.Greenhouses;
import com.wasteofplastic.greenhouses.PlayerCache;
import com.wasteofplastic.greenhouses.greenhouse.Greenhouse;
import com.wasteofplastic.greenhouses.ui.Locale;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class AdmininfoCommand extends Subcommand {


    public AdmininfoCommand(Greenhouses plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return Locale.adminHelpinfo;
    }

    @Override
    public String getUsage(String lable) {
        return "/" + lable + " adminInfo";
    }

    @Override
    public String getPermission() {
        return "greenhouses.admin";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        final PlayerCache players = plugin.players;

        if (args.length == 1) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + Locale.admininfoerror);
                return true;
            }
            Player player = (Player)sender;
            Greenhouse greenhouse = players.getInGreenhouse(player);
            if (greenhouse == null) {
                sender.sendMessage(ChatColor.RED + Locale.admininfoerror2);
                return true;
            }
            sender.sendMessage(ChatColor.GREEN + Locale.infoinfo);
            sender.sendMessage(ChatColor.GREEN + Locale.generalowner + ":" + greenhouse.getPlayerName());
            sender.sendMessage(ChatColor.GREEN + Locale.admininfoflags);
            for (String flag : greenhouse.getFlags().keySet()) {
                sender.sendMessage(flag + ": " + greenhouse.getFlags().get(flag));
            }
            return true;
        } else if (args.length == 2) {
            sender.sendMessage(ChatColor.GREEN + Locale.infoinfo);
            int index = 0;
            boolean found = false;
            for (Greenhouse g : plugin.getGreenhouses()) {
                if (g.getPlayerName().equalsIgnoreCase(args[1])) {
                    if (!found)
                        sender.sendMessage(ChatColor.GREEN + Locale.generalowner + ":" + g.getPlayerName());
                    found = true;
                    sender.sendMessage("Greenhouse #" + (++index));
                    sender.sendMessage("Biome: " + g.getBiome().name());
                    sender.sendMessage("Recipe: " + g.getBiomeRecipe().getFriendlyName());
                    sender.sendMessage(g.getWorld().getName());
                    sender.sendMessage(g.getPos1().getBlockX() + ", " + g.getPos1().getBlockZ() + " to " + g.getPos2().getBlockX() + ", " + g.getPos2().getBlockZ());
                    sender.sendMessage("Base at " + g.getPos1().getBlockY());
                    sender.sendMessage("Height = " + g.getHeight());
                    sender.sendMessage("Area = " + g.getArea());
                }
            }
            if (found) {
                if (index == 0) {
                    sender.sendMessage("Player has no greenhouses.");
                } else {
                    Player player = plugin.getServer().getPlayer(args[1]);
                    if (player != null) {
                        sender.sendMessage("Player has " + index + " greenhouses and is allowed to build " + plugin.getMaxGreenhouses(player));
                    } else {
                        sender.sendMessage("Player has " + index + " greenhouses. Player is offline.");
                    }
                }
            } else {
                sender.sendMessage(ChatColor.RED + "Cannot find that player. (May not have logged on recently)");
            }
            return true;
        } else {
            sender.sendMessage(ChatColor.RED + Locale.errorunknownCommand);
            return false;
        }
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String lable, String[] args) {
        return null;
    }
}