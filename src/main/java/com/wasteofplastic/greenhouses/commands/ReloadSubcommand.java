package com.wasteofplastic.greenhouses.commands;

import com.wasteofplastic.greenhouses.Greenhouses;
import com.wasteofplastic.greenhouses.ui.Locale;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.util.List;

public class ReloadSubcommand extends Subcommand {


    public ReloadSubcommand(Greenhouses plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return Locale.adminHelpreload;
    }

    @Override
    public String getUsage(String lable) {
        return "/" + lable + " reload";
    }

    @Override
    public String getPermission() {
        return "greenhouses.admin";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args[0].equalsIgnoreCase("reload")) {
            plugin.reloadConfig();
            plugin.loadPluginConfig();
            plugin.loadBiomeRecipes();
            plugin.ecoTick();
            sender.sendMessage(ChatColor.YELLOW + Locale.reloadconfigReloaded);
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String lable, String[] args) {
        return null;
    }
}