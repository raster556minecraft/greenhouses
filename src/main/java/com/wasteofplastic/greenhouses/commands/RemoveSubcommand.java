package com.wasteofplastic.greenhouses.commands;

import com.wasteofplastic.greenhouses.Greenhouses;
import com.wasteofplastic.greenhouses.PlayerCache;
import com.wasteofplastic.greenhouses.greenhouse.Greenhouse;
import com.wasteofplastic.greenhouses.ui.Locale;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

public class RemoveSubcommand extends Subcommand {


    public RemoveSubcommand(Greenhouses plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return Locale.helpremove;
    }

    @Override
    public String getUsage(String lable) {
        return "/" + lable + " remove";
    }

    @Override
    public String getPermission() {
        return "greenhouses.player";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        final PlayerCache players = plugin.players;
        final Player player = (Player) sender;
        final UUID playerUUID = player.getUniqueId();

        final Greenhouse greenhouseNow = players.getInGreenhouse(player);
        if (greenhouseNow != null) {
            if (greenhouseNow.getOwner().equals(playerUUID)) {
                player.sendMessage(ChatColor.RED + Locale.errorremoving);
                plugin.removeGreenhouse(greenhouseNow);
                return true;
            }
            player.sendMessage(ChatColor.RED + Locale.errornotyours);
        } else {
            player.sendMessage(ChatColor.RED + Locale.errornotinside);
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String lable, String[] args) {
        return null;
    }
}