package com.wasteofplastic.greenhouses.commands;

import com.wasteofplastic.greenhouses.Greenhouses;
import com.wasteofplastic.greenhouses.ui.Locale;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class HelpSubcommand extends Subcommand {


    public HelpSubcommand(Greenhouses plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return Locale.helphelp;
    }

    @Override
    public String getUsage(String lable) {
        return "/" + lable + " help";
    }

    @Override
    public String getPermission() {
        return "greenhouses.player";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        final Player player = (Player) sender;

        player.sendMessage(" ");
        player.sendMessage(ChatColor.GREEN + Locale.generalgreenhouses +" " + plugin.getDescription().getVersion() + " help:");

        for (Subcommand subcommand: plugin.subCommands.values()) {
            if (player.hasPermission(subcommand.getPermission()))
                player.sendMessage(ChatColor.YELLOW + subcommand.getUsage(label) + " : " + subcommand.getHelp());
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String lable, String[] args) {
        return null;
    }
}
