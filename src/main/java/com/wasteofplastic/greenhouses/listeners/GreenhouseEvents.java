package com.wasteofplastic.greenhouses.listeners;

import com.wasteofplastic.greenhouses.Greenhouses;
import com.wasteofplastic.greenhouses.Settings;
import com.wasteofplastic.greenhouses.greenhouse.Greenhouse;
import com.wasteofplastic.greenhouses.ui.Locale;
import com.wasteofplastic.greenhouses.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.block.data.Waterlogged;
import org.bukkit.block.data.type.Slab;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * @author tastybento
 * This class listens for changes to greenhouses and reacts to them
 */
public class GreenhouseEvents implements Listener {
    private final Greenhouses plugin;

    public GreenhouseEvents(final Greenhouses plugin) {
        this.plugin = plugin;

    }

    /**
     * Permits water to be placed in the Nether if in a greenhouse and in an acceptable biome
     * The water placement is designs to act as close to vanila as possible as of MC version 1.13.2
     * @param event PlayerInteractEvent
     */
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled=true)
    public void onPlayerInteract(PlayerInteractEvent event){
        Player player = event.getPlayer();
        World world = player.getWorld();
        EntityType fishType = null;

        // Check we are in the right world for nether water placement
        if (world.getEnvironment().equals(Environment.NETHER) && Settings.worldName.contains(world.getName())) {

            if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
                Biome biome = Objects.requireNonNull(event.getClickedBlock()).getBiome();
                if (event.getItem() != null) {

                    Block block = event.getClickedBlock();
                    Block relBlock = event.getClickedBlock().getRelative(event.getBlockFace());
                    Material itemHand = event.getItem().getType();

                    if (plugin.getInGreenhouse(block.getLocation()) == null || plugin.getInGreenhouse(relBlock.getLocation()) == null) {
                        return;
                    }

                    // Allow pouring of water if biome is okay
                    if ((itemHand.equals(Material.WATER_BUCKET) || itemHand.equals(Material.COD_BUCKET) || itemHand.equals(Material.PUFFERFISH_BUCKET) ||
                            itemHand.equals(Material.SALMON_BUCKET) || itemHand.equals(Material.TROPICAL_FISH_BUCKET)) &&
                            !biome.equals(Biome.NETHER) && !biome.equals(Biome.DESERT) && !biome.equals(Biome.DESERT_HILLS)) {
                        event.setCancelled(true);

                        switch (itemHand) {
                            case COD_BUCKET:
                                fishType = EntityType.COD;
                                break;
                            case PUFFERFISH_BUCKET:
                                fishType = EntityType.PUFFERFISH;
                                break;
                            case SALMON_BUCKET:
                                fishType = EntityType.SALMON;
                                break;
                            case TROPICAL_FISH_BUCKET:
                                fishType = EntityType.TROPICAL_FISH;
                                break;
                        }

                        // Place in a water as close to vanila as possible
                        // If the block is waterlogged we want to get that watterloged data
                        if (block.getBlockData() instanceof Waterlogged) {
                            Waterlogged waterLoggedBlock = (Waterlogged) block.getBlockData();
                            // If the block is not waterlogged then we want to fill the block as long as it is not a double slab
                            if (!waterLoggedBlock.isWaterlogged()) {
                                // If the block is a slab get the slab data
                                if (waterLoggedBlock instanceof Slab) {
                                    Slab slab = (Slab) waterLoggedBlock;
                                    // Double slabs can be waterlogged but it is not normal behavior so we make sure it is not double
                                    if (!slab.getType().equals(Slab.Type.DOUBLE)) {
                                        waterLoggedBlock.setWaterlogged(true);
                                        block.setBlockData(waterLoggedBlock);
                                        // Spawn fish
                                        if (fishType != null) {
                                            block.getWorld().spawnEntity(block.getLocation().add(0.5, 0.5, 0.5), fishType);
                                        }
                                        return;
                                    }
                                    // If we were not a slab we want to go through the waterlogging without a slab check
                                } else {
                                    waterLoggedBlock.setWaterlogged(true);
                                    block.setBlockData(waterLoggedBlock);
                                    // Spawn fish
                                    if (fishType != null) {
                                        block.getWorld().spawnEntity(block.getLocation().add(0.5, 0.5, 0.5), fishType);
                                    }
                                    return;
                                }
                            }
                        }
                        // We only get here if the block was waterlogged or was a solid block. Now we fill the agacent block with water if air
                        if (relBlock.getType().equals(Material.AIR) || relBlock.getType().equals(Material.VOID_AIR) || relBlock.getType().equals(Material.CAVE_AIR)) {
                            relBlock.setType(Material.WATER, true);
                            // Spawn fish
                            if (fishType != null) {
                                relBlock.getWorld().spawnEntity(relBlock.getLocation().add(0.5, 0.5, 0.5), fishType);
                            }
                            return;
                        }
                        // If it was not air but is waterloggable we waterlog it
                        if (relBlock.getBlockData() instanceof Waterlogged) {
                            Waterlogged waterLoggedBlock = (Waterlogged) relBlock.getBlockData();
                            // Same slab check
                            if (waterLoggedBlock instanceof Slab) {
                                Slab slab = (Slab) waterLoggedBlock;
                                if (!slab.getType().equals(Slab.Type.DOUBLE)) {
                                    waterLoggedBlock.setWaterlogged(true);
                                    relBlock.setBlockData(waterLoggedBlock);
                                    // ensure we are not registering a task while the plugin is dissabled
                                    if (plugin.isEnabled()) {
                                        Bukkit.getScheduler().runTask(plugin, () -> relBlock.getState().update(true, true));
                                    }
                                    // Spawn fish
                                    if (fishType != null) {
                                        relBlock.getWorld().spawnEntity(relBlock.getLocation().add(0.5, 0.5, 0.5), fishType);
                                    }
                                    return;
                                }
                                // Run if not slab
                            } else {
                                waterLoggedBlock.setWaterlogged(true);
                                relBlock.setBlockData(waterLoggedBlock);
                                // ensure we are not registering a task while the plugin is dissabled
                                if (plugin.isEnabled()) {
                                    Bukkit.getScheduler().runTask(plugin, () -> relBlock.getState().update(true, true));
                                }
                                // Spawn fish
                                if (fishType != null) {
                                    relBlock.getWorld().spawnEntity(relBlock.getLocation().add(0.5, 0.5, 0.5), fishType);
                                }
                                return;
                            }
                            // Finaly if the others were false we want to fill the rel block as long as it is not solid and naturaly break the block
                        }
                        if (!relBlock.getType().isSolid()) {
                            relBlock.breakNaturally();
                            relBlock.setType(Material.WATER);
                            // Spawn fish
                            if (fishType != null) {
                                relBlock.getWorld().spawnEntity(relBlock.getLocation().add(0.5, 0.5, 0.5), fishType);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Makes water in the Nether if ice is broken and in a greenhouse
     * @param event BlockBreakEvent
     */
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled=true)
    public void onIceBreak(BlockBreakEvent event){
        Player player = event.getPlayer();
        World world = player.getWorld();
        // Check we are in the right world
        if (!Settings.worldName.contains(world.getName())) {
            return;
        }
        Biome biome = event.getBlock().getBiome();
        // Set to water if the biome is okay.
        if(event.getBlock().getWorld().getEnvironment() == Environment.NETHER && event.getBlock().getType() == Material.ICE
                && !biome.equals(Biome.NETHER) && !biome.equals(Biome.DESERT) && !biome.equals(Biome.DESERT_HILLS)) {
            event.setCancelled(true);
            event.getBlock().setType(Material.WATER);
        }
    }

    /*
    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled=true)
    public void onPlayerMove(PlayerMoveEvent event) {
        //plugin.logger(3,event.getEventName());
        Player player = event.getPlayer();
        UUID uuid = player.getUniqueId();
        if (plugin.getPlayerGHouse(uuid) == null || plugin.getPlayerGHouse(uuid).isEmpty()) {
            return;
        }

        World world = player.getWorld();
        // Check we are in the right world
        if (!Settings.worldName.contains(world.getName())) {
            plugin.logger(4,"Not in a Greenhouse world");
            return;
        }
        // Did we move a block?
        if (event.getFrom().getBlockX() != event.getTo().getBlockX()
                || event.getFrom().getBlockY() != event.getTo().getBlockY()
                || event.getFrom().getBlockZ() != event.getTo().getBlockZ()) {
            boolean result = checkMove(player, event.getFrom(), event.getTo(), uuid);
            if (result) {
                Location newLoc = event.getFrom();
                newLoc.setX(newLoc.getBlockX() + 0.5);
                newLoc.setY(newLoc.getBlockY());
                newLoc.setZ(newLoc.getBlockZ() + 0.5);
                event.setTo(newLoc);
            }
        }
    }


    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        // Strangely, sometimes these worlds can be null
        if (event.getFrom() == null || event.getTo() == null) {
            return;
        }
        // Check if they changed worlds
        UUID uuid = event.getPlayer().getUniqueId();
        if (plugin.getPlayerGHouse(uuid) == null || plugin.getPlayerGHouse(uuid).isEmpty()) {
            return;
        }

        World fromWorld = event.getFrom().getWorld();
        World toWorld = event.getTo().getWorld();
        // Check we are in the right world
        if (!Settings.worldName.contains(fromWorld.getName()) && !Settings.worldName.contains(toWorld.getName())) {
            return;
        }
        // Did we move a block?
        checkMove(event.getPlayer(), event.getFrom(), event.getTo(), uuid);
    }
     */
    /*
    private boolean checkMove(Player player, Location from, Location to, UUID uuid) {
        Greenhouse fromGreenhouse = null;
        Greenhouse toGreenhouse= null;
        if (plugin.getGreenhouses().isEmpty()) {
            // No greenhouses yet
            return false;
        }
        plugin.logger(4,"Checking greenhouses");
        plugin.logger(4,"From : " + from.toString());
        plugin.logger(4,"From: " + from.getBlockX() + "," + from.getBlockZ());
        plugin.logger(4,"To: " + to.getBlockX() + "," + to.getBlockZ());
        for (Greenhouse d: plugin.getPlayerGHouse(uuid)) {
            plugin.logger(4,"Greenhouse (" + d.getPos1().getBlockX() + "," + d.getPos1().getBlockZ() + " : " + d.getPos2().getBlockX() + "," + d.getPos2().getBlockZ() + ")");
            if (d.insideGreenhouse(to)) {
                plugin.logger(4,"To intersects d!");
                toGreenhouse = d;
            }
            if (d.insideGreenhouse(from)) {
                plugin.logger(4,"From intersects d!");
                fromGreenhouse = d;
            }


        }
        // No greenhouse interaction
        if (fromGreenhouse == null && toGreenhouse == null) {
            // Clear the greenhouse flag (the greenhouse may have been deleted while they were offline)
            plugin.players.setInGreenhouse(player, null);
            return false;
        } else if (fromGreenhouse == toGreenhouse) {
            // Set the greenhouse - needs to be done if the player teleports too (should be done on a teleport event)
            plugin.players.setInGreenhouse(player, toGreenhouse);
            return false;
        }
        if (fromGreenhouse != null && toGreenhouse == null) {
            // leaving a greenhouse
            if (!fromGreenhouse.getFarewellMessage().isEmpty()) {
                player.sendMessage(fromGreenhouse.getFarewellMessage());
            }
            plugin.players.setInGreenhouse(player, null);
            //if (plugin.players.getNumberInGreenhouse(fromGreenhouse) == 0) {
            //	fromGreenhouse.ƒ();
            //}
        } else if (fromGreenhouse == null && toGreenhouse != null){
            // Going into a greenhouse
            if (!toGreenhouse.getEnterMessage().isEmpty()) {
                player.sendMessage(toGreenhouse.getEnterMessage());

                //plugin.visualize(toGreenhouse, player);
            }
            toGreenhouse.startBiome(false);
            plugin.players.setInGreenhouse(player, toGreenhouse);

        } else if (fromGreenhouse != null && toGreenhouse != null){
            // Leaving one greenhouse and entering another greenhouse immediately
            if (!fromGreenhouse.getFarewellMessage().isEmpty()) {
                player.sendMessage(fromGreenhouse.getFarewellMessage());
            }
            plugin.players.setInGreenhouse(player, toGreenhouse);
            //if (plugin.players.getNumberInGreenhouse(fromGreenhouse) == 0) {
            //fromGreenhouse.endBiome();
            //}
            toGreenhouse.startBiome(false);
            if (!toGreenhouse.getEnterMessage().isEmpty()) {
                player.sendMessage(toGreenhouse.getEnterMessage());
            }
        }
        return false;
    }
     */

    /**
     * Checks if broken blocks cause the greenhouse to fail
     * @param e BlockBreakEvent
     */
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled=true)
    public void onBlockBreak(final BlockBreakEvent e) {
        if (!Settings.worldName.contains(e.getPlayer().getWorld().getName())) {
            return;
        }
        plugin.logger(3,"block break");
        // Get the greenhouse that this block is in (if any)
        Greenhouse g = plugin.getInGreenhouse(e.getBlock().getLocation());

        if (g == null) {
            // Not in a greenhouse
            plugin.logger(3,"not in greenhouse");
            return;
        }
        // Check to see if this causes the greenhouse to break
        if ((e.getBlock().getLocation().getBlockY() == g.getHeightY()) || (g.isAWall(e.getBlock().getLocation()))) {
            e.getPlayer().sendMessage(ChatColor.RED + Locale.eventbroke.replace("[biome]",Util.prettifyText(g.getOriginalBiome().toString())));
            e.getPlayer().sendMessage(ChatColor.RED + Locale.eventfix);
            plugin.removeGreenhouse(g);
        }
    }

    /**
     * Checks if exploded blocks cause the greenhouse to fail
     * @param e EntityExplodeEvent
     */
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled=true)
    public void onEntityExplode(final EntityExplodeEvent e) {
        if (!Settings.worldName.contains(Objects.requireNonNull(e.getLocation().getWorld()).getName())) {
            return;
        }

        List<Block> blocks = e.blockList(); // Get the exploded blocks

        for (Block block: blocks) {

            plugin.logger(3, "entity explode block break");
            // Get the greenhouse that this block is in (if any)

            Greenhouse g = plugin.getInGreenhouse(block.getLocation().add(0.5, 0.5, 0.5));

            if (g == null) {
                // Not in a greenhouse
                plugin.logger(3, "not in greenhouse");
                continue;
            }
            // Check to see if this causes the greenhouse to break
            if ((block.getLocation().getBlockY() == g.getHeightY()) || (g.isAWall(block.getLocation()))) {
                plugin.removeGreenhouse(g);
                Player player = Bukkit.getPlayer(g.getOwner());
                Objects.requireNonNull(player).sendMessage(ChatColor.RED + Locale.eventbroke.replace("[biome]",Util.prettifyText(g.getOriginalBiome().toString())));
                player.sendMessage(ChatColor.RED + Locale.eventfix);
            }
        }
    }

    /**
     * Checks if exploded blocks cause the greenhouse to fail
     * @param e BlockExplodeEvent
     */
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled=true)
    public void onBlockExplode(final BlockExplodeEvent e) {
        if (!Settings.worldName.contains(e.getBlock().getWorld().getName())) {
            return;
        }

        List<Block> blocks = e.blockList(); // Get the exploded blocks

        for (Block block: blocks) {

            plugin.logger(3, "block explode block break");
            // Get the greenhouse that this block is in (if any)

            Greenhouse g = plugin.getInGreenhouse(block.getLocation().add(0.5, 0.5, 0.5));

            if (g == null) {
                // Not in a greenhouse
                plugin.logger(3, "not in greenhouse");
                continue;
            }
            // Check to see if this causes the greenhouse to break
            if ((block.getLocation().getBlockY() == g.getHeightY()) || (g.isAWall(block.getLocation()))) {
                plugin.removeGreenhouse(g);
                Player player = Bukkit.getPlayer(g.getOwner());
                Objects.requireNonNull(player).sendMessage(ChatColor.RED + Locale.eventbroke.replace("[biome]",Util.prettifyText(g.getOriginalBiome().toString())));
                player.sendMessage(ChatColor.RED + Locale.eventfix);
            }
        }
    }

    /**
     * Checks if piston extention causes the greenhouse to fail
     * @param e BlockPistonExtendEvent
     */
    @EventHandler(ignoreCancelled=true)
    public void onPistonExtend(final BlockPistonExtendEvent e) {
        if (!Settings.worldName.contains(e.getBlock().getWorld().getName())) {
            return;
        }

        List<Block> blocks = e.getBlocks(); // Get the piston blocks

        // Test wall break
        for (Block block: blocks) {
            plugin.logger(3, "piston block move");
            // Get the greenhouse that this block is in (if any)

            // See if the block infront is above a greenhouse
            if (plugin.aboveAGreenhouse(block.getRelative(e.getDirection()).getLocation()) != null) {
                Collection<Entity> entites = block.getWorld().getNearbyEntities(block.getLocation(), 25, 25, 25);

                plugin.logger(3,"Piston atempted to move above greenhouse");
                plugin.logger(3,"Direction: " + e.getDirection());
                plugin.logger(3,"Location of piston block: " + e.getBlock().getX() + ", " + e.getBlock().getY() + ", "  + e.getBlock().getZ());

                for (Entity entity: entites) {
                    if (entity instanceof Player) {
                        Player player = (Player) entity;
                        player.sendMessage(ChatColor.RED + Locale.eventpistonerror);
                    }
                }
                e.setCancelled(true); // Cancel because we are above a greenhouse
                return;
            }

            Greenhouse g = plugin.getInGreenhouse(block.getLocation());

            if (g == null) {
                // Not in a greenhouse
                plugin.logger(3, "not in greenhouse");
                continue;
            }

            // Check to see if this causes the greenhouse to break
            if ((block.getLocation().getBlockY() == g.getHeightY()) || ((g.isAWall(block.getLocation())))) {
                plugin.removeGreenhouse(g);
                Player player = Bukkit.getPlayer(g.getOwner());
                Objects.requireNonNull(player).sendMessage(ChatColor.RED + Locale.eventbroke.replace("[biome]",Util.prettifyText(g.getOriginalBiome().toString())));
                player.sendMessage(ChatColor.RED + Locale.eventfix);
            }
        }
    }

    /**
     * Checks if piston retraction causes the greenhouse to fail
     * @param e BlockPistonRetractEvent
     */
    @EventHandler(ignoreCancelled=true)
    public void onPistonRetract(final BlockPistonRetractEvent e) {
        if (!Settings.worldName.contains(e.getBlock().getWorld().getName())) {
            return;
        }

        List<Block> blocks = e.getBlocks(); // Get the piston blocks

        // Test wall break
        for (Block block: blocks) {
            plugin.logger(3, "piston block move");
            // Get the greenhouse that this block is in (if any)

            // See if the block infront is above a greenhouse
            if (plugin.aboveAGreenhouse(block.getRelative(e.getDirection()).getLocation()) != null) {
                Collection<Entity> entites = block.getWorld().getNearbyEntities(block.getLocation(), 25, 25, 25);

                plugin.logger(3,"Piston atempted to move above greenhouse");
                plugin.logger(3,"Direction: " + e.getDirection());
                plugin.logger(3,"Location of piston block: " + e.getBlock().getX() + ", " + e.getBlock().getY() + ", "  + e.getBlock().getZ());

                for (Entity entity: entites) {
                    if (entity instanceof Player) {
                        Player player = (Player) entity;
                        player.sendMessage(ChatColor.RED + Locale.eventpistonerror);
                    }
                }
                e.setCancelled(true); // Cancel because we are above a greenhouse
                return;
            }

            Greenhouse g = plugin.getInGreenhouse(block.getLocation());

            if (g == null) {
                // Not in a greenhouse
                plugin.logger(3, "not in greenhouse");
                continue;
            }

            // Check to see if this causes the greenhouse to break
            if ((block.getLocation().getBlockY() == g.getHeightY()) || ((g.isAWall(block.getLocation())))) {
                plugin.removeGreenhouse(g);
                Player player = Bukkit.getPlayer(g.getOwner());
                Objects.requireNonNull(player).sendMessage(ChatColor.RED + Locale.eventbroke.replace("[biome]",Util.prettifyText(g.getOriginalBiome().toString())));
                player.sendMessage(ChatColor.RED + Locale.eventfix);
            }
        }
    }

    /**
     * Prevents placing of blocks above the greenhouses
     * @param e BlockPlaceEvent
     */
    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled=true)
    public void onPlayerBlockPlace(final BlockPlaceEvent e) {
        if (!Settings.worldName.contains(e.getPlayer().getWorld().getName())) {
            return;
        }
        if (e.getPlayer().getWorld().getEnvironment().equals(Environment.NETHER)) {
            return;
        }
        // If the offending block is not above a greenhouse, forget it!
        Greenhouse g = plugin.aboveAGreenhouse(e.getBlock().getLocation());
        if (g == null) {
            return;
        }
        e.getPlayer().sendMessage(ChatColor.RED + Locale.eventcannotplace);
        //e.getPlayer().sendMessage("Greenhouse is at " + g.getPos1() + " to " + g.getPos2());
        e.setCancelled(true);
    }
}

