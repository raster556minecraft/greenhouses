package com.wasteofplastic.greenhouses.greenhouse;

import com.wasteofplastic.greenhouses.Greenhouses;
import com.wasteofplastic.greenhouses.ui.Locale;
import com.wasteofplastic.greenhouses.util.Util;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.Ageable;
import org.bukkit.block.data.Bisected;
import org.bukkit.block.data.Bisected.Half;
import org.bukkit.block.data.Levelled;
import org.bukkit.block.data.Waterlogged;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class BiomeRecipe {
    private final Greenhouses plugin;
    private Biome type;
    private Material icon; // Biome icon for control panel
    private int priority;
    private String name;
    private String friendlyName;
    // Content requirements
    // Material, Qty. There can be more than one type of material required
    private final List<List<Material>> blockMaterials = new ArrayList<>();
    private final List<Long> blockQty  = new ArrayList<>();
    private final List<Long> blockQtyCheck  = new ArrayList<>();
    // Plants
    // Plant Material, Material on which to grow, Probability
    private final List<Material> plantMaterial = new ArrayList<>();
    private final List<Integer> plantProbability = new ArrayList<>();
    private final List<Material> plantGrownOn = new ArrayList<>();
    // Mobs
    // Entity Type, Material to Spawn on, Probability
    private final List<EntityType> mobType = new ArrayList<>();
    private final List<Double> mobProbability = new ArrayList<>();
    private final List<Material> mobSpawnOn = new ArrayList<>();
    // Conversions
    // Original Material, New Material, Probability
    private final List<Material> oldMaterial = new ArrayList<>();
    private final List<Integer> convChance = new ArrayList<>();
    private final List<Material> newMaterial = new ArrayList<>();
    private final List<Material> localMaterial = new ArrayList<>();

    private int mobLimit;
    private int waterCoverage;
    private int iceCoverage;
    private int lavaCoverage;

    private String permission = "";

    /**
     * @param plugin The plugin instance
     * @param type The Biome in the recipe
     * @param priority Priority of the biome recipe
     */
    public BiomeRecipe(Greenhouses plugin, Biome type, int priority) {
        this.plugin = plugin;
        this.type = type;
        this.priority = priority;
        plugin.logger(3, "" + type.toString() + " priority " + priority);
        mobLimit = 9; // Default
    }

    public void addReqBlocks(List<Material> blockMaterials, long blockQty) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < blockMaterials.size(); i++) {
            ItemStack item = new ItemStack(blockMaterials.get(i), 1);
            if (i != 0) { sb.append(" or "); }
            sb.append(Util.getName(item));
        }
        plugin.logger(1, "   " + sb.toString() + " x " + blockQty);
        this.blockMaterials.add(blockMaterials);
        this.blockQty.add(blockQty);
        this.blockQtyCheck.add(blockQty);
    }

    // Check required blocks
    /**
     * Returns true if a cube defined by pos1 and pos2 meet this biome recipe. If
     * player is not null, a explaination of any failures will be provided.
     *
     * @param pos1 First corner of the greenhouse
     * @param pos2 Second cornter of the greenhouse
     * @param player The player making the greenhouse
     * @return True if pass and false if failed
     */

    public boolean checkRecipe(Location pos1, Location pos2, Player player) {
        plugin.logger(3, "Checking for biome " + type.toString());
        long area = (pos2.getBlockX() - pos1.getBlockX() - 1) * (pos2.getBlockZ() - pos1.getBlockZ() - 1);
        plugin.logger(3, "area =" + area);
        plugin.logger(3, "Pos1 = " + pos1.toString());
        plugin.logger(3, "Pos1 = " + pos2.toString());
        long water = 0;
        long lava = 0;
        long ice = 0;
        boolean pass = true;
        // Look through the greenhouse and count what is in there
        for (int y = pos1.getBlockY(); y < pos2.getBlockY(); y++) {
            for (int x = pos1.getBlockX() + 1; x < pos2.getBlockX(); x++) {
                for (int z = pos1.getBlockZ() + 1; z < pos2.getBlockZ(); z++) {
                    Block b = Objects.requireNonNull(pos1.getWorld()).getBlockAt(x, y, z);

                    if (!(b.getType().equals(Material.AIR) || b.getType().equals(Material.CAVE_AIR) || b.getType().equals(Material.VOID_AIR)))
                        plugin.logger(3,
                                "Checking block " + b.getType() + "@" + x + " " + y + " " + z);
                    // Log water, lava and ice blocks
                    switch (b.getType()) {
                    case WATER:
                    case BUBBLE_COLUMN:
                    case SEAGRASS:
                    case TALL_SEAGRASS:
                    case KELP_PLANT:
                    case KELP:
                        water++;
                        break;
                    case LAVA:
                        lava++;
                        break;
                    case ICE:
                    case PACKED_ICE:
                    case BLUE_ICE:
                    case FROSTED_ICE:
                        ice++;
                        break;
                    default:
                        // If block is waterlogged add to water count
                        if (b.getBlockData() instanceof Waterlogged) {
                            Waterlogged waterLoggedBlock = (Waterlogged) b.getBlockData();
                            if (waterLoggedBlock.isWaterlogged())
                                water++;
                        }
                        break;
                    }

                    int index = indexOfReqBlocks(b.getType());
                    if (index >= 0) {
                        plugin.logger(3, "Found block " + b.getType().toString() + " at index "
                                + index);
                        // Decrement the qty
                        this.blockQtyCheck.set(index, this.blockQtyCheck.get(index) - 1L);
                    }
                }
            }
        }
        // Calculate % water, ice and lava ratios
        double waterRatio = (double) water / (double) area * 100;
        double lavaRatio = (double) lava / (double) area * 100;
        double iceRatio = (double) ice / (double) area * 100;
        plugin.logger(3, "water count=" + water);
        plugin.logger(3, "water req=" + waterCoverage + " lava req=" + lavaCoverage + " ice req=" + iceCoverage);
        plugin.logger(3, "waterRatio=" + waterRatio + " lavaRatio=" + lavaRatio + " iceRatio=" + iceRatio);

        // Check required ratios - a zero means none of these are allowed, e.g.desert
        // has no water
        if (waterCoverage == 0 && waterRatio > 0) {
            if (player != null) {
                player.sendMessage(ChatColor.RED + Locale.recipenowater);
            }
            pass = false;
        }
        if (lavaCoverage == 0 && lavaRatio > 0) {
            if (player != null) {
                player.sendMessage(ChatColor.RED + Locale.recipenolava);
            }
            pass = false;
        }
        if (iceCoverage == 0 && iceRatio > 0) {
            if (player != null) {
                player.sendMessage(ChatColor.RED + Locale.recipenoice);
            }
            pass = false;
        }
        if (waterCoverage > 0 && waterRatio < waterCoverage) {
            if (player != null) {
                player.sendMessage(
                        ChatColor.RED + Locale.recipewatermustbe.replace("[coverage]", String.valueOf(waterCoverage)));
            }
            pass = false;
        }
        if (lavaCoverage > 0 && lavaRatio < lavaCoverage) {
            if (player != null) {
                player.sendMessage(
                        ChatColor.RED + Locale.recipelavamustbe.replace("[coverage]", String.valueOf(lavaCoverage)));
            }
            pass = false;

        }
        if (iceCoverage > 0 && iceRatio < iceCoverage) {
            if (player != null) {
                player.sendMessage(
                        ChatColor.RED + Locale.recipeicemustbe.replace("[coverage]", String.valueOf(iceCoverage)));
            }
            pass = false;
        }
        // Every value in the blockQtyCheck list should be zero or negative
        // Now check if the minimum block qtys are met and reset the check qtys
        plugin.logger(3, "checking blocks - total size is " + blockQty.size());
        for (int i = 0; i < this.blockQty.size(); i++) {
            if (this.blockQtyCheck.get(i) > 0L) {
                StringBuilder sb = new StringBuilder();
                for (int f = 0; f < blockMaterials.get(i).size(); f++) {
                    if (f != 0) { sb.append(" or "); }
                    sb.append(Util.getName( new ItemStack(blockMaterials.get(i).get(f), 1)));
                }
                plugin.logger(3,
                        "missing: " + blockQtyCheck.get(i) + " x " + sb.toString());
                pass = false;
                if (player != null) {

                    player.sendMessage(ChatColor.RED + Locale.recipemissing + " " + blockQtyCheck.get(i) + " x "
                            + sb.toString());
                }

            }

            // reset the list
            this.blockQtyCheck.set(i, this.blockQty.get(i));
        } /*
             * if (pass) plugin.logger(3,"DEBUG: Could be biome " +
             * type.toString()); else plugin.logger(3,"DEBUG: Cannot be biome " +
             * type.toString());
             */
        return pass;
    }

    private int indexOfReqBlocks(Material blockMaterial) {
        // Leaves need special handling because their state can change
        plugin.logger(3, "looking for block " + blockMaterial.toString());
        boolean foundBlock = false;
        for (List<Material> blockMaterialSublist: blockMaterials) {
            if (blockMaterialSublist.contains(blockMaterial)) {
                foundBlock = true;
                break;
            }
        }
        if (!foundBlock)
            return -1;
        int index = 0;
        for (List<Material> m : blockMaterials) {
            if (m.contains(blockMaterial)) {
                return index;
            }
            index++;
        }
        return -1; // This should never be needed...
    }

    /**
     * @return A list of blocks that are required for this recipe
     */
    public List<String> getRecipeBlocks() {
        List<String> blocks = new ArrayList<>();
        int index = 0;
        for (List<Material> m : blockMaterials) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < m.size(); i++) {
                ItemStack item = new ItemStack(m.get(i), 1);
                if (i != 0) { sb.append(" or "); }
                sb.append(Util.getName(item));
            }

            blocks.add(sb.toString() + " x " + blockQty.get(index));
            index++;
        }
        return blocks;
    }

    /**
     * Creates a list of plants that can grow, the probability and what they must
     * grow on. Data is drawn from the file biomes.yml
     *
     * @param plantMaterial The material of the plant
     * @param plantProbability The probability of spawning
     * @param plantGrowOn The material the plant needs to grow on
     */
    public void addPlants(Material plantMaterial, int plantProbability, Material plantGrowOn) {
        ItemStack i = new ItemStack(plantMaterial);

        plugin.logger(1, "   " + plantProbability + "% chance for " + Util.getName(i) + " to grow.");
        this.plantMaterial.add(plantMaterial);
        this.plantProbability.add(plantProbability);
        this.plantGrownOn.add(plantGrowOn);
    }

    public void addMobs(EntityType mobType, int mobProbability, Material mobSpawnOn) {
        plugin.logger(1, "   " + mobProbability + "% chance for " + Util.prettifyText(mobType.toString())
                + " to spawn on " + Util.prettifyText(mobSpawnOn.toString()) + ".");
        this.mobType.add(mobType);
        double probability = ((double) mobProbability / 100);
        // this.mobProbability.add(((double)mobProbability/100));
        this.mobSpawnOn.add(mobSpawnOn);
        // Add up all the probabilities in the list so far
        double totalProb = 0D;
        for (double prob : this.mobProbability) {
            totalProb += prob;
        }
        if ((1D - totalProb) >= probability) {
            this.mobProbability.add(probability);
        } else {
            plugin.getLogger().warning("   Mob chances add up to >100% in " + type.toString() + " biome recipe!");
        }
    }

    public EntityType getMob() {
        // Return a random mob that can spawn in the biome or null
        double rand = Math.random();
        plugin.logger(3, "random number is " + rand);
        double runningTotal = 0D;
        int index = 0;
        for (double prob : mobProbability) {
            runningTotal += prob;
            plugin.logger(3, "running total is " + runningTotal);
            if (rand < runningTotal) {
                plugin.logger(3, "hit! " + mobType.get(index).toString());
                return mobType.get(index);
            }
            index++;
        }
        return null;
    }

    /**
     * @param mobType Mob type to check
     * @return the Material on which this type of mob must spawn on in this biome
     */
    public Material getMobSpawnOn(EntityType mobType) {
        int index = this.mobType.indexOf(mobType);
        if (index == -1)
            return null;
        return this.mobSpawnOn.get(index);
    }

    /**
     * @return The mobLimit
     */
    public int getMobLimit() {
        return mobLimit;
    }

    /**
     * @param mobLimit The mobLimit to set
     */
    public void setMobLimit(int mobLimit) {
        this.mobLimit = mobLimit;
    }

    public void addConvBlocks(Material oldMaterial, Material newMaterial, int convChance,
            Material localMaterial) {
        this.oldMaterial.add(oldMaterial);
        this.newMaterial.add(newMaterial);
        this.localMaterial.add(localMaterial);
        this.convChance.add(convChance);
    }

    /**
     * @return True if there are blocks to convert for this biome
     */
    public boolean getBlockConvert() {
        return !oldMaterial.isEmpty();
    }

    public void convertBlock(Block b) {
        plugin.logger(3, "try to convert block");
        // Check if block is in the list
        if (!oldMaterial.contains(b.getType())) {
            plugin.logger(3, "no material or type match");
            return;
        }
        plugin.logger(3, "material or type match");
        int index = oldMaterial.indexOf(b.getType());
        // Block material match
        // Check the chance
        double chance = Math.random();
        double convCh = (double) convChance.get(index) / 100D;
        if (chance > convCh) {
            plugin.logger(3, "failed the probability check - " + chance + " > " + convCh);
            return;
        }
        plugin.logger(3, "pass the probability check");
        // Check if the block is in the right area, up, down, n,s,e,w
        if (localMaterial.get(index) != null) {
            plugin.logger(3, "Looking for " + localMaterial.get(index).toString());
            boolean found = false;
            for (BlockFace bf : BlockFace.values()) {
                switch (bf) {
                case DOWN:
                case EAST:
                case NORTH:
                case SOUTH:
                case UP:
                case WEST:
                    plugin.logger(3, bf.toString() + " material is " + b.getRelative(bf).getType().toString());
                    if (b.getRelative(bf).getType().equals(localMaterial.get(index))) {
                        plugin.logger(3, "Material matches");
                        plugin.logger(3, "found adjacent block");
                        found = true;
                        break;
                    }
                    break;
                default:
                    break;

                }

            }
            if (!found)
                return;
        } else {
            plugin.logger(3, "no adjacent block requirement");
        }

        // Convert!
        plugin.logger(3, "Convert block");
        b.setType(newMaterial.get(index), false);
    }

    /**
     * @return The type
     */
    public Biome getBiome() {
        return type;
    }

    /**
     * @return The priority
     */
    public int getPriority() {
        return priority;
    }

    /**
     * @return The waterCoverage
     */
    public int getWaterCoverage() {
        return waterCoverage;
    }

    /**
     * @return The iceCoverage
     */
    public int getIceCoverage() {
        return iceCoverage;
    }

    /**
     * @return The lavaCoverage
     */
    public int getLavaCoverage() {
        return lavaCoverage;
    }

    /**
     * @return List of plants
     */
    public List<Material> getPlants() {
        return plantMaterial;
    }

    /**
     * @return List of plant probability
     */
    public List<Integer> getPlantProbabilities() {
        return plantProbability;
    }

    /**
     * @return List of reqired blocks for plants
     */
    public List<Material> getPlantRequiredBlocks() {
        return plantGrownOn;
    }

    /**
     * @return Old blocks for converting
     */
    public List<Material> getConvertOld() {
        return oldMaterial;
    }

    /**
     * @return Materials required to convert
     */
    public List<Material> getConvertRequired() {
        return localMaterial;
    }

    /**
     * @return New blocks for converting
     */
    public List<Material> getConvertNew() {
        return newMaterial;
    }

    /**
     * @return Chances of convert
     */
    public List<Integer> getConvertChance() {
        return convChance;
    }

    /**
     * @return Mobs that spawn
     */
    public List<EntityType> getMobSpawns() {
        return mobType;
    }

    /**
     * @return Blocks the mobs can spawn on
     */
    public List<Material> getBlockSpawnOn() {
        return mobSpawnOn;
    }

    /**
     * @return Chances of mob spawn
     */
    public List<Double> getSpawnChance() {
        return mobProbability;
    }

    /**
     * @param type The type to set
     */
    public void setType(Biome type) {
        this.type = type;
    }

    /**
     * @param priority The priority to set
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    /**
     * @param watercoverage The waterCoverage to set
     */
    public void setWatercoverage(int watercoverage) {
        if (watercoverage == 0) {
            plugin.logger(1, "   No Water Allowed");
        } else if (watercoverage > 0) {
            plugin.logger(1, "   Water > " + watercoverage + "%");
        }
        this.waterCoverage = watercoverage;
    }

    /**
     * @param icecoverage The icecoverage to set
     */
    public void setIcecoverage(int icecoverage) {
        if (icecoverage == 0) {
            plugin.logger(1, "   No Ice Allowed");
        } else if (icecoverage > 0) {
            plugin.logger(1, "   Ice > " + icecoverage + "%");
        }
        this.iceCoverage = icecoverage;
    }

    /**
     * @param lavacoverage The lavaCoverage to set
     */
    public void setLavacoverage(int lavacoverage) {
        if (lavacoverage == 0) {
            plugin.logger(1, "   No Lava Allowed");
        } else if (lavacoverage > 0) {
            plugin.logger(1, "   Lava > " + lavacoverage + "%");
        }
        this.lavaCoverage = lavacoverage;
    }

    /**
     * Plants a plant on block bl if it makes sense.
     *
     * @param bl Block to grow plant on
     * @return True if plant grew and false if growth failed
     */
    public boolean growPlant(Block bl) {
        if (!(bl.getType() == Material.AIR || bl.getType() == Material.VOID_AIR || bl.getType() == Material.CAVE_AIR)) {
            return false;
        }

        // Grow crops by default minecraft bonmeal rules
        Block blBelow = bl.getRelative(BlockFace.DOWN);
        // Grow crops
        if ((blBelow.getType() == Material.WHEAT || blBelow.getType() == Material.CARROTS || blBelow.getType() == Material.POTATOES ||
                blBelow.getType() == Material.PUMPKIN_STEM || blBelow.getType() == Material.MELON_STEM)) {
            if (blBelow.getBlockData() instanceof Ageable) {
                Ageable crop = (Ageable) blBelow.getState().getBlockData();
                if (crop.getAge() >= 3) {return false;}
                int newAge = crop.getAge() + (int) Math.round(Math.random() * 3 + 2);
                if (newAge > 7) {
                    newAge = 7;
                }
                crop.setAge(newAge);
                blBelow.setBlockData(crop, true);
            }
            blBelow.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, blBelow.getLocation(), 3, 1, 1, 1);
            return false; // No normal person would want plants to grow on plants

        } else if ((blBelow.getType() == Material.BEETROOTS)) {
            if (blBelow.getBlockData() instanceof Ageable) {
                if (Math.random() < .75) {
                    Ageable crop = (Ageable) blBelow.getState().getBlockData();
                    if (crop.getAge() >= 3) {return false;}
                    int newAge = crop.getAge() + 1;
                    if (newAge > 3) {
                        newAge = 3;
                    }
                    crop.setAge(newAge);
                    blBelow.setBlockData(crop, true);
                }
            }
            blBelow.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, blBelow.getLocation(), 3, 1, 1, 1);
            return false; // No normal person would want plants to grow on plants
        }

        // Plants a plant on block bl if it make sense
        // Loop through the possible plants
        boolean grewPlant = false;
        int index = 0;
        plugin.logger(3, "growPlant # of plants in biome = " + plantProbability.size());
        for (int prob : plantProbability) {
            plugin.logger(3, "probability = " + ((double) prob / 100));

            if (Math.random() < ((double) prob / 100)) {
                plugin.logger(2, "trying to grow plant. Index is " + index);
                // Okay worth trying to plant something
                Block belowBl = bl.getRelative(BlockFace.DOWN);
                Block aboveBl = bl.getRelative(BlockFace.UP);
                Block northBl = bl.getRelative(BlockFace.NORTH);
                Block eastBl = bl.getRelative(BlockFace.EAST);
                Block southBl = bl.getRelative(BlockFace.SOUTH);
                Block westBl = bl.getRelative(BlockFace.WEST);

                plugin.logger(3, "material found = " + belowBl.getType().toString());
                plugin.logger(3, "above = " + aboveBl.getType().toString());
                plugin.logger(3, "req material = " + plantGrownOn.get(index).toString());
                if (belowBl.getType() == plantGrownOn.get(index)) {

                    // Check if we are spawning in water
                    if (plantGrownOn.get(index) == Material.WATER) {
                        // Get the levelled object
                        if (belowBl.getBlockData() instanceof Levelled) {
                            Levelled water = (Levelled) belowBl.getBlockData();
                            // If our water is not stationary
                            if (water.getLevel() != 0) {
                                continue;
                            }
                        }
                    }

                    Material pMat = (plantMaterial.get(index));
                    plugin.logger(2, "Plant Material | " + pMat.toString());
                    if (!(pMat.equals(Material.SUNFLOWER)
                            || pMat.equals(Material.LILAC)
                            || pMat.equals(Material.ROSE_BUSH)
                            || pMat.equals(Material.PEONY)
                            || pMat.equals(Material.TALL_GRASS)
                            || pMat.equals(Material.LARGE_FERN))) {

                        // Mushrooms have special lighting and block conditions
                        if ((pMat == Material.BROWN_MUSHROOM || pMat == Material.RED_MUSHROOM) && !(belowBl.getType() == Material.PODZOL || belowBl.getType() == Material.MYCELIUM)) {
                            if (bl.getLightLevel() >= (byte)13) {
                                return grewPlant;
                            }
                        }

                        // Cacti need room to grow or it will pop
                        if (pMat == Material.CACTUS) {
                            if (!((northBl.getType().equals(Material.AIR) || northBl.getType().equals(Material.VOID_AIR) || northBl.getType().equals(Material.CAVE_AIR)) &&
                                    (eastBl.getType().equals(Material.AIR) || eastBl.getType().equals(Material.VOID_AIR) || eastBl.getType().equals(Material.CAVE_AIR)) &&
                                    (southBl.getType().equals(Material.AIR) || southBl.getType().equals(Material.VOID_AIR) || southBl.getType().equals(Material.CAVE_AIR)) &&
                                    (westBl.getType().equals(Material.AIR) || westBl.getType().equals(Material.VOID_AIR) || westBl.getType().equals(Material.CAVE_AIR)))) {
                                continue;
                            }
                        }
                        bl.setType(pMat, false);
                        grewPlant = true;
                    } else {
                        // Check if there is room above for the plant
                        plugin.logger(2, "Double plant time!");
                        if (aboveBl.getType() == Material.AIR) {
                            plugin.logger(2, "Above above is AIR!");
                            bl.setType(pMat, false);
                            aboveBl.setType(pMat, false);
                            Bisected bisected = (Bisected) pMat.createBlockData();
                            bisected.setHalf(Half.BOTTOM);
                            Bisected bisectedTop = (Bisected) pMat.createBlockData();
                            bisectedTop.setHalf(Half.TOP);
                            bl.setBlockData(bisected, false);
                            aboveBl.setBlockData(bisectedTop, false);
                            grewPlant = true;
                        } else {
                            plugin.logger(3, "Above above is not AIR");
                        }
                    }

                }
            }
            index++;
        }
        return grewPlant;
    }

    /**
     * @return The icon
     */
    public Material getIcon() {
        return icon;
    }

    /**
     * @param icon The icon to set
     */
    public void setIcon(Material icon) {
        this.icon = icon;
    }

    /**
     * @return The permission
     */
    public String getPermission() {
        return permission;
    }

    /**
     * @param permission The permission to set
     */
    public void setPermission(String permission) {
        this.permission = permission;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The friendly name
     */
    public String getFriendlyName() {
        return friendlyName;
    }

    /**
     * @param friendlyName The friendly name
     */
    public void setFriendlyName(String friendlyName) {
        this.friendlyName = friendlyName;
    }

}
