package com.wasteofplastic.greenhouses;

import com.wasteofplastic.greenhouses.commands.Subcommand;
import com.wasteofplastic.greenhouses.ui.Locale;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CommandManager implements CommandExecutor, TabCompleter {


    private Greenhouses plugin;

    public CommandManager(Greenhouses plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String lable, String[] args) {

        HashMap<String, Subcommand> subcommands = plugin.subCommands;
        Subcommand subcommand;

        if (args.length == 0) {
            subcommand = subcommands.get("gui");
        } else {
            subcommand = subcommands.get(args[0]);
            if (subcommand == null)
                subcommand = subcommands.get("help");
        }
        if (!sender.hasPermission(subcommand.getPermission())) {
            sender.sendMessage(ChatColor.RED + Locale.errornoPermission);
            return true;
        }
        return subcommand.onCommand(sender, cmd, lable, args);
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String lable, String[] args) {
        HashMap<String, Subcommand> subcommands = plugin.subCommands;
        Subcommand subcommand;

        if (args.length == 1) {
            List<String> keySet  = new ArrayList<>(subcommands.keySet());
            List<String> tabList = new ArrayList<>();
            for (String key : keySet) {
                if (sender.hasPermission(subcommands.get(key).getPermission()))
                    tabList.add(key);
            }

            return tabList;
        } else {
            subcommand = subcommands.get(args[0]);

            if (subcommand == null)
                subcommand = subcommands.get("help");
        }

        return subcommand.onTabComplete(sender, cmd, lable, args);

    }
}