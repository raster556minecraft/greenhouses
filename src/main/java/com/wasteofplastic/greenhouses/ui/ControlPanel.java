package com.wasteofplastic.greenhouses.ui;

import com.wasteofplastic.greenhouses.Greenhouses;
import com.wasteofplastic.greenhouses.greenhouse.BiomeRecipe;
import com.wasteofplastic.greenhouses.greenhouse.Greenhouse;
import com.wasteofplastic.greenhouses.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;


/**
 * @author tastybento
 * Provides a handy control panel
 */

public class ControlPanel implements Listener {

    private final Greenhouses plugin;
    public Inventory detectionInventory;
    public Inventory ecologyInventory;

    private final HashMap<UUID, HashMap<Integer,BiomeRecipe>> biomeCreatePanels = new HashMap<>();
    private final HashMap<UUID, BiomeRecipe> detectBiomeRecipeCache = new HashMap<>();
    /**
     * @param plugin
     * Greenhouses class
     */
    public ControlPanel(Greenhouses plugin) {
        this.plugin = plugin;
        detectionInventory = getDetectionPanel();
        ecologyInventory = getBiomeEcologyPanel();
    }

    // Prevent a possible memory leak
    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        Player player = (Player) event.getPlayer();
        plugin.uiCache.delMenuUI(player.getUniqueId());
        plugin.uiCache.delBiomeCreateUI(player.getUniqueId());
        detectBiomeRecipeCache.remove(player.getUniqueId()); // Remove Cached biome recipe
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        plugin.uiCache.delMenuUI(player.getUniqueId());
        plugin.uiCache.delBiomeCreateUI(player.getUniqueId());
        detectBiomeRecipeCache.remove(player.getUniqueId()); // Remove Cached biome recipe
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked(); // The player that clicked the item
        Inventory inventory = event.getInventory(); // The inventory that was clicked in
        int slot = event.getRawSlot();

        // Check this is the Greenhouse Detection Sensitivy Selection Panel
        if (inventory.equals(detectionInventory)) {
            event.setCancelled(true); // Don't let them pick anything up
            BiomeRecipe biomeRecipe = detectBiomeRecipeCache.get(player.getUniqueId()); // Get Cached biome recipe

            if (slot < 0)  {
                player.closeInventory();
                return;
            }

            if (slot == 2) {
                // Sets up a greenhouse in low sensitivity
                detectBiomeRecipeCache.remove(player.getUniqueId()); // Remove Cached biome recipe
                Greenhouse oldg = plugin.players.getInGreenhouse(player);
                // Check ownership
                if (oldg != null && !oldg.getOwner().equals(player.getUniqueId())) {
                    player.sendMessage(Locale.errornotyours);
                    player.closeInventory();

                    return;
                }
                if (oldg != null) {
                    // Player wants to try and change biome
                    //player.closeInventory(); // Closes the inventory
                    // error.exists
                    //player.sendMessage(ChatColor.RED + Locale.erroralreadyexists);
                    //return;
                    plugin.removeGreenhouse(oldg);
                }
                // Check if they are at their limit
                if (plugin.players.isAtLimit(player)) {
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', Locale.infonomore));
                } else {
                    // Make greenhouse
                    Greenhouse g = plugin.tryToMakeGreenhouse(player, biomeRecipe, false);
                    if (g == null) {
                        player.closeInventory(); // Closes the inventory
                        //error.norecipe
                        player.sendMessage(ChatColor.RED + Locale.errornorecipe);
                        return;
                    }
                    player.closeInventory(); // Closes the inventory
                }
                //player.performCommand("greenhouse make");
                //player.sendMessage(message);
                return;
            }

            if (slot == 6) {
                // Sets up a greenhouse in low sensitivity
                detectBiomeRecipeCache.remove(player.getUniqueId()); // Remove Cached biome recipe
                Greenhouse oldg = plugin.players.getInGreenhouse(player);
                // Check ownership
                if (oldg != null && !oldg.getOwner().equals(player.getUniqueId())) {
                    player.sendMessage(Locale.errornotyours);
                    player.closeInventory();

                    return;
                }
                if (oldg != null) {
                    // Player wants to try and change biome
                    //player.closeInventory(); // Closes the inventory
                    // error.exists
                    //player.sendMessage(ChatColor.RED + Locale.erroralreadyexists);
                    //return;
                    plugin.removeGreenhouse(oldg);
                }
                // Check if they are at their limit
                if (plugin.players.isAtLimit(player)) {
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', Locale.infonomore));
                } else {
                    // Make greenhouse
                    Greenhouse g = plugin.tryToMakeGreenhouse(player, biomeRecipe, true);
                    if (g == null) {
                        player.closeInventory(); // Closes the inventory
                        //error.norecipe
                        player.sendMessage(ChatColor.RED + Locale.errornorecipe);
                        return;
                    }
                    player.closeInventory(); // Closes the inventory
                }
                //player.performCommand("greenhouse make");
                //player.sendMessage(message);
            }
        }

        // Check this is the Greenhouse Menu Panel
        if (inventory.equals(plugin.uiCache.getMenuUI(player.getUniqueId()))) {
            event.setCancelled(true); // Don't let them pick anything up

            if (slot < 0)  {
                player.closeInventory();
                return;
            }

            if (slot == 1) {
               // Run the gh info command
                player.performCommand("gh info");
                player.closeInventory();
            }

            if (slot == 3) {
                // Run the gh remove command
                player.performCommand("gh remove");
                player.closeInventory();
            }

            if (slot == 5) {
                // Open biome create inventory
                player.closeInventory();
                Inventory playerInv = plugin.getRecipeInv(player);
                plugin.uiCache.storeBiomeCreateUI(player.getUniqueId(), playerInv);
                player.openInventory(playerInv);
            }

            if (slot == 7) {
                // Open biome ecology inventory
                player.closeInventory();
                player.openInventory(ecologyInventory);
            }

        }

        // Check this is the Ecology Panel
        if (inventory.equals(ecologyInventory)) {
            event.setCancelled(true); // Don't let them pick anything up
            if (slot < 0)  {
                player.closeInventory();
                return;
            }

            if (slot < plugin.getBiomeRecipes().size()) {
                player.closeInventory(); // Closes the inventory
                player.performCommand("gh ecology " + (slot+1));
            }
        }

        // Check this is the Greenhouse Biome Creation Panel
        if (inventory.equals(plugin.uiCache.getBiomeCreateUI(player.getUniqueId()))) {
            event.setCancelled(true); // Don't let them pick anything up
            if (slot < 0)  {
                player.closeInventory();
                return;
            }

            HashMap<Integer,BiomeRecipe> panel = biomeCreatePanels.get(player.getUniqueId());
            if (panel == null) {
                // No panel, so just return
                return;
            }

            if (panel.containsKey(slot)) {

                BiomeRecipe biomeRecipe = panel.get(slot);
                player.closeInventory(); // Closes the inventory
                detectBiomeRecipeCache.put(player.getUniqueId(), biomeRecipe);
                player.openInventory(detectionInventory);
            }
        }
    }

    /**
     * Creates a player-specific biome panel based on permissions
     * @param player Player to get the panel from
     *
     * @return The biomeCreatePanel Inventory
     */
    public Inventory getBiomeCreatePanel(Player player) {
        HashMap<Integer, BiomeRecipe> store = new HashMap<>();
        int index = 0;
        // Run through biomes and add to the inventory if this player is allowed to use them
        for (BiomeRecipe br : plugin.getBiomeRecipes()) {
            // Gather the info
            if (br.getPermission().isEmpty() || player.hasPermission(br.getPermission())) {
                // Add this biome recipe to the list
                store.put(index++, br);
            }
        }
        // Now create the panel
        //int panelSize = store.size() + 9 - 1;
        int panelSize = store.size() + 9;
        panelSize -= ( panelSize % 9);
        Inventory biomeCreatePanel = Bukkit.createInventory(player, panelSize, ChatColor.translateAlternateColorCodes('&', Locale.controlpaneltitle));
        
        // Now add the biomes
        index = 0;
        for (BiomeRecipe br : store.values()) {
            // Create an itemStack
            ItemStack item = new ItemStack(br.getIcon());
            ItemMeta meta = item.getItemMeta();
            if (br.getFriendlyName().isEmpty()) {
                Objects.requireNonNull(meta).setDisplayName(Util.prettifyText(br.getBiome().toString()));
            } else {
                Objects.requireNonNull(meta).setDisplayName(br.getFriendlyName());
            }
            List<String> lore = new ArrayList<>();
            List<String> reqBlocks = br.getRecipeBlocks();
            if (reqBlocks.size() > 0) {
                lore.add(ChatColor.YELLOW + Locale.recipeminimumblockstitle);
                int i = 1;
                for (String list : reqBlocks) {
                    lore.add(Locale.lineColor + (i++) + ": " + list);
                }
            } else {
                lore.add(ChatColor.YELLOW + Locale.recipenootherblocks);
            }
            if (br.getWaterCoverage() == 0) {
                lore.add(Locale.recipenowater);
            } else if (br.getWaterCoverage() > 0) {
                lore.add(Locale.recipewatermustbe.replace("[coverage]", String.valueOf(br.getWaterCoverage())));
            }
            if (br.getIceCoverage() == 0) {
                lore.add(Locale.recipenoice);
            } else if (br.getIceCoverage() > 0) {
                lore.add(Locale.recipeicemustbe.replace("[coverage]", String.valueOf(br.getIceCoverage())));
            }
            if (br.getLavaCoverage() == 0) {
                lore.add(Locale.recipenolava);
            } else if (br.getLavaCoverage() > 0) {
                lore.add(Locale.recipelavamustbe.replace("[coverage]", String.valueOf(br.getLavaCoverage())));
            }
            meta.setLore(lore);
            item.setItemMeta(meta);
            biomeCreatePanel.setItem(index++,item);
        }

        // the spacers
        ItemStack item = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
        ItemMeta meta = item.getItemMeta();
        Objects.requireNonNull(meta).setDisplayName(" ");
        item.setItemMeta(meta);

        for (int i = index; i < panelSize; i++) {
            biomeCreatePanel.setItem(i, item);
        }

        // Stash the panel for later use when clicked
        biomeCreatePanels.put(player.getUniqueId(), store);
        return biomeCreatePanel;
    }

    /**
     * Creates an ecology panel
     *
     * @return The ecologyPanel Inventory
     */
    public Inventory getBiomeEcologyPanel() {
        HashMap<Integer, BiomeRecipe> store = new HashMap<>();
        int index = 0;
        // Run through biomes and add to the inventory if this player is allowed to use them
        for (BiomeRecipe br : plugin.getBiomeRecipes()) {
            // Gather the info
            store.put(index++, br);
        }
        // Now create the panel
        //int panelSize = store.size() + 9 - 1;
        int panelSize = store.size() + 9;
        panelSize -= ( panelSize % 9);
        Inventory ecologyPanel = Bukkit.createInventory(null, panelSize, ChatColor.translateAlternateColorCodes('&', Locale.controlpaneltitle));

        // Now add the biomes
        index = 0;
        for (BiomeRecipe br : store.values()) {
            // Create an itemStack
            ItemStack item = new ItemStack(br.getIcon());
            ItemMeta meta = item.getItemMeta();
            if (br.getFriendlyName().isEmpty()) {
                Objects.requireNonNull(meta).setDisplayName(Util.prettifyText(br.getBiome().toString()));
            } else {
                Objects.requireNonNull(meta).setDisplayName(br.getFriendlyName());
            }
            List<String> lore = new ArrayList<>();
            lore.add("Click to see ecology info");
            meta.setLore(lore);
            item.setItemMeta(meta);
            ecologyPanel.setItem(index++,item);
        }

        // the spacers
        ItemStack item = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
        ItemMeta meta = item.getItemMeta();
        Objects.requireNonNull(meta).setDisplayName(" ");
        item.setItemMeta(meta);

        for (int i = index; i < panelSize; i++) {
            ecologyPanel.setItem(i, item);
        }

        return ecologyPanel;
    }

    /**
     * Creates a player-specific menu panel based on permissions
     * @param player Player to get the panel from
     *
     * @return The menuPanel Inventory
     */
    
    public Inventory getMenuPanel(Player player) {

        // create the panel
        int panelSize = 9;
        Inventory menuPanel = Bukkit.createInventory(null, panelSize, ChatColor.translateAlternateColorCodes('&', Locale.controlpaneltitle));
        // Now add the buttons

        // the spacers
        ItemStack item = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
        ItemMeta meta = item.getItemMeta();
        Objects.requireNonNull(meta).setDisplayName(" ");
        item.setItemMeta(meta);
        menuPanel.setItem(0, item);
        menuPanel.setItem(2, item);
        menuPanel.setItem(4, item);
        menuPanel.setItem(6, item);
        menuPanel.setItem(8, item);

        // Add the instructions item
        item = new ItemStack(Material.KNOWLEDGE_BOOK);
        meta = item.getItemMeta();
        Objects.requireNonNull(meta).setDisplayName(Locale.generalgreenhouses);
        List<String> lore = new ArrayList<>(Arrays.asList(Locale.infowelcome.split("\\|")));
        if (plugin.players.isAtLimit(player)) {
            lore.add(ChatColor.translateAlternateColorCodes('&', Locale.infonomore));
        } else {
            if (plugin.players.getRemainingGreenhouses(player) > 0) {
                if (plugin.players.getRemainingGreenhouses(player) == 1) {
                    lore.addAll(new ArrayList<>(Arrays.asList(Locale.infoonemore.split("\\|"))));
                } else {
                    String temp = Locale.infoyoucanbuild.replace("[number]", String.valueOf(plugin.players.getRemainingGreenhouses(player)));
                    lore.addAll(new ArrayList<>(Arrays.asList(temp.split("\\|"))));
                }
            } else {
                lore.addAll(new ArrayList<>(Arrays.asList(Locale.infounlimited.split("\\|"))));
            }
        }
        meta.setLore(lore);
        item.setItemMeta(meta);
        menuPanel.setItem(1, item);
        
        // GH remove button
        item = new ItemStack(Material.RED_CONCRETE);
        meta = item.getItemMeta();
        Objects.requireNonNull(meta).setDisplayName(ChatColor.RED +  "Remove Greenhouse");
        item.setItemMeta(meta);
        lore.clear();
        lore.add("Must be in your greenhosue to remove it");
        meta.setLore(lore);
        item.setItemMeta(meta);
        menuPanel.setItem(3, item);

        // GH create button
        item = new ItemStack(Material.LIME_CONCRETE);
        meta = item.getItemMeta();
        Objects.requireNonNull(meta).setDisplayName(ChatColor.GREEN +  "Create Greenhouse");
        lore.clear();
        lore.add("Create a greenhouse");
        meta.setLore(lore);
        item.setItemMeta(meta);
        menuPanel.setItem(5, item);

        // GH ecology button
        item = new ItemStack(Material.GRASS_BLOCK);
        meta = item.getItemMeta();
        Objects.requireNonNull(meta).setDisplayName(ChatColor.DARK_GREEN +  "Biome Ecology");
        lore.clear();
        lore.add("Get more information about each biome");
        meta.setLore(lore);
        item.setItemMeta(meta);
        menuPanel.setItem(7, item);

        return menuPanel;
    }

    private Inventory getDetectionPanel() {

        // create the panel
        int panelSize = 9;
        Inventory detectionPanel = Bukkit.createInventory(null, panelSize, ChatColor.translateAlternateColorCodes('&', Locale.controlpaneltitle));
        // Now add the buttons

        // the spacers
        ItemStack item = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
        ItemMeta meta = item.getItemMeta();
        Objects.requireNonNull(meta).setDisplayName(" ");
        item.setItemMeta(meta);
        detectionPanel.setItem(0, item);
        detectionPanel.setItem(1, item);
        detectionPanel.setItem(3, item);
        detectionPanel.setItem(4, item);
        detectionPanel.setItem(5, item);
        detectionPanel.setItem(7, item);
        detectionPanel.setItem(8, item);

        // normal detect button
        item = new ItemStack(Material.YELLOW_CONCRETE);
        meta = item.getItemMeta();
        Objects.requireNonNull(meta).setDisplayName(ChatColor.YELLOW +  "Normal Detect");
        item.setItemMeta(meta);
        List<String> lore = new ArrayList<>();
        lore.add("Detect by finding glass");
        meta.setLore(lore);
        item.setItemMeta(meta);
        detectionPanel.setItem(2, item);

        // strict detect button
        item = new ItemStack(Material.RED_CONCRETE);
        meta = item.getItemMeta();
        Objects.requireNonNull(meta).setDisplayName(ChatColor.RED +  "Strict Detect");
        lore.clear();
        lore.add("Detect by finding all viable blocks");
        lore.add("Use this if normal mode fails to work");
        lore.add(ChatColor.GRAY + "Must remove Hoppers, Glowstone, and");
        lore.add(ChatColor.GRAY + "Doors from inside for this mode to work");
        meta.setLore(lore);
        item.setItemMeta(meta);
        detectionPanel.setItem(6, item);

        return detectionPanel;
    }
}
