package com.wasteofplastic.greenhouses.ui;

import com.wasteofplastic.greenhouses.Greenhouses;
import org.bukkit.inventory.Inventory;

import java.util.HashMap;
import java.util.UUID;


/**
 * @author Raster556
 * Where some UI of the plugin is cached
 */

public class UICache {

    private final Greenhouses plugin;
    private final HashMap<UUID, Inventory> biomeCreateInv = new HashMap<>();
    private final HashMap<UUID, Inventory> menuInv = new HashMap<>();

    /**
     * @param plugin The Greenhouses classpe
     */

    public UICache(Greenhouses plugin) {
        this.plugin = plugin;
    }


    /**
     * Store the BiomeCreateUI in a cache for future use
     * @param playerUUID The player's UUID that the inventory belongs to
     * @param inventory The inventory to store
     */
    // Cache the biomeCreate ui
    public void storeBiomeCreateUI (UUID playerUUID, Inventory inventory) {
        biomeCreateInv.put(playerUUID, inventory);
    }

    /**
     * Retrive the BiomeCreateUI from the cache
     * @param playerUUID The player's UUID that the inventory belongs to
     * @return The inventory
     */
    // Retrive the  biomeCreate ui
    public Inventory getBiomeCreateUI (UUID playerUUID) {
        return biomeCreateInv.get(playerUUID);
    }

    /**
     * Remove the BiomeCreateUI from the cache
     * @param playerUUID The player's UUID that the inventory belongs to
     * @return True if the UI existed in the Cache and false if it didn't
     */
    // Remove the biomeCreate ui
    public boolean delBiomeCreateUI (UUID playerUUID) {
        return biomeCreateInv.remove(playerUUID) != null;
    }


    /**
     * Store the MenuUI in a cache for future use
     * @param playerUUID The player's UUID that the inventory belongs to
     * @param inventory The inventory to store
     */
    // Cache the biomeCreate ui
    public void storeMenuUI (UUID playerUUID, Inventory inventory) {
        menuInv.put(playerUUID, inventory);
    }

    /**
     * Retrive the MenuUI from the cache
     * @param playerUUID The player's UUID that the inventory belongs to
     * @return The inventory
     */
    // Retrive the  biomeCreate ui
    public Inventory getMenuUI (UUID playerUUID) {
        return menuInv.get(playerUUID);
    }

    /**
     * Remove the MenuUI from the cache
     * @param playerUUID The player's UUID that the inventory belongs to
     * @return True if the UI existed in the Cache and false if it didn't
     */
    // Remove the biomeCreate ui
    public boolean delMenuUI (UUID playerUUID) {
        return menuInv.remove(playerUUID) != null;
    }
}
