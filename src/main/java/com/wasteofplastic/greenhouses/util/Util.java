package com.wasteofplastic.greenhouses.util;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.map.MinecraftFont;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Util {

    /**
     * Fetches an ItemStack's name - For example, converting INK_SAC:11 to
     * Dandelion Yellow, or WOOL:14 to Red Wool
     *
     * @param i
     *            The itemstack to fetch the name of
     * @return The human readable item name.
     */
    public static String getName(ItemStack i) {
        // If the item has had its name changed, then let's use that
        String vanillaName;
        String displayName = Objects.requireNonNull(i.getItemMeta()).getDisplayName();
        if (i.getItemMeta().hasDisplayName()) {
            vanillaName = displayName;
        } else {
            vanillaName = prettifyText(getDataName(i));
        }
        return vanillaName;
    }

    private static final String[] ROMAN = { "X", "IX", "V", "IV", "I" };
    private static final int[] DECIMAL = { 10, 9, 5, 4, 1 };

    /**
     * Converts the given number to roman numerals. If the number is &gt;= 40 or &lt;=
     * 0, it will just return the number as a string.
     *
     * @param n
     *            The number to convert
     * @return The roman numeral representation of this number, or the number in
     *         decimal form as a string if n &lt;= 0 || n &gt;= 40.
     */
    public static String toRoman(int n) {
        if (n <= 0 || n >= 40)
            return "" + n;
        StringBuilder roman = new StringBuilder();

        for (int i = 0; i < ROMAN.length; i++) {
            while (n >= DECIMAL[i]) {
                n -= DECIMAL[i];
                roman.append(ROMAN[i]);
            }
        }

        return roman.toString();
    }

    /**
     * Converts a name like IRON_INGOT into Iron Ingot to improve readability
     *
     * @param ugly
     *            The string such as IRON_INGOT
     * @return A nicer version, such as Iron Ingot
     *
     *         Credits to mikenon on GitHub!
     */
    public static String prettifyText(String ugly) {
        if (!ugly.contains("_") && (!ugly.equals(ugly.toUpperCase())))
            return ugly;
        StringBuilder fin = new StringBuilder();
        ugly = ugly.toLowerCase();
        if (ugly.contains("_")) {
            String[] splt = ugly.split("_");
            int i = 0;
            for (String s : splt) {
                i += 1;
                fin.append(Character.toUpperCase(s.charAt(0))).append(s.substring(1));
                if (i < splt.length)
                    fin.append(" ");
            }
        } else {
            fin.append(Character.toUpperCase(ugly.charAt(0))).append(ugly.substring(1));
        }
        return fin.toString();
    }

    /**
     * Converts a given ItemStack into a pretty string
     *
     * @param item
     *            The item stack
     * @return A string with the name of the item.
     */
    private static String getDataName(ItemStack item) {
        Material mat = item.getType();
        return mat.toString();
    }

    /**
     * Chops up a long line into shorter lengths while at the same time
     * preserving the chat color
     * @param longLine The long line text
     * @param length The length of the new text
     * @return The shortened text
     */
    static List<String> chop(String longLine, int length) {
        // Use this to check lengths
        MinecraftFont mcf = new MinecraftFont();
        List<String> result = new ArrayList<>();
        if (!mcf.isValid(longLine)) {
            result.add(longLine);
            return result;
        }
        // Go through letter by letter
        // This is the current line that is being built
        StringBuilder currentLine = new StringBuilder();
        // Last chat color
        String color = "";
        List<String> formatting = new ArrayList<>();
        char[] line = longLine.toCharArray();
        for (int i = 0; i< line.length; i++) {
            // Chat color check
            while (line[i] == ChatColor.COLOR_CHAR) {
                // Found a color or formatting
                // Record this color or formatting
                if (i+1 < line.length) {
                    // Avoid any problems should this symbol just so happen to be at the end of the string
                    // Formatting
                    if (line[i+1] == 'k' || line[i+1] == 'l' || line[i+1] == 'm' || line[i+1] == 'n' || line[i+1] == 'o') {
                        formatting.add(String.copyValueOf(line, i, 2));
                    } else if (line[i+1] == 'r') {
                        // Reset clears all colors and formatting
                        color = "";
                        formatting.clear();
                    } else {
                        // Colors replace each other
                        if (!color.isEmpty()) {
                            formatting.remove(String.copyValueOf(line, i, 2));
                        }
                        formatting.add(String.copyValueOf(line, i, 2));
                        color = String.copyValueOf(line, i, 2);
                    }
                }
                // Colors and formatting do not add to line width
                // Add this color/formatting code
                currentLine.append(String.copyValueOf(line, i, 2));
                // Skip ahead past this code
                i = i + 2;
            }
            // Check if we are adding a space to the start of a new line
            if (!(ChatColor.stripColor(currentLine.toString()).isEmpty() && line[i] == ' ')) {
                currentLine.append(line[i]);
            }
            //Bukkit.getLogger().info("DEBUG " + ChatColor.stripColor(currentLine) + " length =" + mcf.getWidth(ChatColor.stripColor(currentLine)));

            if (mcf.getWidth(ChatColor.stripColor(currentLine.toString())) >= length) {
                // Start a new line
                // Word wrap
                // Get last space
                int lastSpace = currentLine.lastIndexOf(" ");
                // If no space, or space is at the end of the line
                if (lastSpace == -1 || lastSpace == currentLine.length()) {
                    // No space found
                    result.add(currentLine.toString());
                    currentLine = new StringBuilder();
                    // Add in any formatting
                    if (!formatting.isEmpty()) {
                        for (String c : formatting) {
                            currentLine.append(c);
                        }
                    }
                } else {
                    // Word wrap
                    result.add(currentLine.substring(0, lastSpace));
                    String newLine = currentLine.substring(lastSpace+1);
                    currentLine = new StringBuilder();
                    // Add in any formatting
                    if (!formatting.isEmpty()) {
                        for (String c : formatting) {
                            currentLine.append(c);
                        }
                    }
                    currentLine.append(newLine);
                }
            }
        }
        if (currentLine.length() > 0) {
            result.add(currentLine.toString());
        }
        return result;
    }

}
