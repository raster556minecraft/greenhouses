package com.wasteofplastic.greenhouses.util;

import java.util.*;

/**
 * @author ben Provides a descending order sort
 */
class MapUtil {
    public static <Key, Value extends Comparable<? super Value>> Map<Key, Value> sortByValue(Map<Key, Value> map) {
        List<Map.Entry<Key, Value>> list = new LinkedList<>(map.entrySet());
        list.sort((o1, o2) -> {
            // Switch these two if you want ascending
            return (o2.getValue()).compareTo(o1.getValue());
        });

        Map<Key, Value> result = new LinkedHashMap<>();
        for (Map.Entry<Key, Value> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }
}
