# This file lists the recipes for the greenhouse biomes
biomes:
  # Biome recipe name - can be anything you like, but must be unique
  beaches:
    # The name of the icon. Can use & for color codes, e.g. &c
    friendlyname: "Beach"
    # The biome of the recipe. Allows multiple recipes for the same biome.
    biome: BEACH
    # The icon is shown in the panel. It must be a Bukkit Material
    icon: SAND
    # Priority is used if the greenhouse can be more than one biome. The highest
    # priority wins
    priority: 0
    # Contents - The minimum requirement for this biome.
    # Format is Material:Number of blocks
    # For multiple options of blocks the format is Material|Material:number
    contents: SAND:1
    # The number of blocks in the greenhouse that must be water, ice or lava
    # Floor area * this % = number of blocks required
    watercoverage: 50
    # If the value is zero, then NO ice/water/lava is allowed
    # If the values are missing, or negative, then ice/water/lava is allowed, but not
    # required for the biome.
    # icecoverage: 0
    # lavacoverage: 0
    # Plants that can grow via the hopper/bonemeal system
    # Format is:
    # Material: % chance:Block type on which it can grow
    # Note that with really small greenhouses, melons and pumpkins can change
    # grass to dirt, which may break the eco system!
    plants:
      DEAD_BUSH: 5:SAND
    # Mobs that may spawn.
    # Format:
    # Entity name: % chance:Block on which the mob will spawn
    mobs:
      SQUID: 10:WATER
    # The minimum number of blocks each mob requires.
    # Mobs will not spawn if there is more than 1 per this number of
    # blocks in the greenhouse. e.g., in this case only 2 mobs will spawn if the
    # greenhouse area is 18 blocks
    # water for mob spawning only counts for water sources
    moblimit: 9
  Snowy_beach:
    friendlyname: "Snowy beach"
    biome: SNOWY_BEACH
    icon: SNOW_BLOCK
    priority: 21
    contents: SAND:1
    watercoverage: 50
    icecoverage: 10
  ThreeWolfMoon:
    friendlyname: "Three Wolf Moon Forest"
    # Could do with more wolves, but the magic works with 3.
    biome: SNOWY_TAIGA
    icon: OAK_SAPLING
    priority: 20
    contents: OAK_LOG|SPRUCE_LOG:3 OAK_LEAVES|SPRUCE_LEAVES:3 GRASS_BLOCK:3
    icecoverage: 10
    plants:
      GRASS: 10:GRASS_BLOCK
    mobs:
      WOLF: 10:SNOW
    moblimit: 9
  Cold_Rabbit:
    friendlyname: "Cold Taiga Forest"
    biome: SNOWY_TAIGA
    icon: OAK_SAPLING
    priority: 20
    contents: OAK_LOG|SPRUCE_LOG:3 OAK_LEAVES|SPRUCE_LEAVES:3 GRASS_BLOCK:3
    icecoverage: 10
    plants:
      GRASS 10:GRASS_BLOCK
    mobs:
      RABBIT: 10:SNOW
    moblimit: 9
  DESERT:
    friendlyname: "Desert"
    biome: DESERT
    icon: DEAD_BUSH
    priority: 3
    contents: SAND:1
    # No water allowed
    watercoverage: 0
    # No ice allowed
    icecoverage: 0
    plants:
      DEAD_BUSH: 10:SAND
      CACTUS: 10:SAND
    # Conversions (see below for another variation)
    # Format is:
    # Original Block MaterialChance of change:New Block Material
    # So, for below, regular dirt has a 30% chance of changing into regular sand.
    conversions: DIRT:30:SAND
  FOREST:
    friendlyname: "Flowery forest"
    biome: FOREST
    icon: POPPY
    priority: 4
    contents: OAK_LOG:3 OAK_LEAVES:4 GRASS_BLOCK:4
    plants:
      OXEYE_DAISY: 2:GRASS_BLOCK
      PEONY: 4:GRASS_BLOCK
      GRASS: 20:GRASS_BLOCK
  HELL:
    friendlyname: "&cNether"
    biome: NETHER
    icon: LAVA_BUCKET
    priority: 5
    contents: NETHERRACK:1
    # Lava required, no ice or water allowed
    lavacoverage: 21
    icecoverage: 0
    watercoverage: 0
    mobs:
      PIG_ZOMBIE: 10:NETHERRACK
    moblimit: 9
    permission: greenhouses.biome.nether
  JUNGLE:
    biome: JUNGLE
    icon: VINE
    priority: 6
    contents: GRASS_BLOCK:4 JUNGLE_LOG:3 JUNGLE_LEAVES:4
    plants:
      DANDELION: 20:GRASS_BLOCK
      MELON: 10:GRASS_BLOCK
      POPPY: 20:GRASS_BLOCK
      LARGE_FERN: 20:GRASS_BLOCK
      FERN: 20:GRASS_BLOCK
  MUSHROOM_ISLAND:
    friendlyname: "Mushroom Island"
    biome: MUSHROOM_FIELDS
    icon: RED_MUSHROOM
    priority: 11
    contents: MYCELIUM:2
    # Water required at 30%
    watercoverage: 30
    plants:
      BROWN_MUSHROOM: 10:MYCELIUM
      RED_MUSHROOM: 10:MYCELIUM
    mobs:
      MUSHROOM_COW: 10:MYCELIUM
    moblimit: 9
  OCEAN:
    biome: OCEAN
    icon: WATER_BUCKET
    priority: 8
    # Lots of water required!
    watercoverage: 95
    mobs:
      SQUID: 10:WATER
    moblimit: 9
  PLAINS:
    friendlyname: "Horse Plains"
    biome: PLAINS
    icon: GRASS_BLOCK
    priority: 1
    contents: GRASS_BLOCK:3
    plants:
      GRASS: 10:GRASS_BLOCK
    mobs:
      HORSE: 10:GRASS_BLOCK
    moblimit: 9
  RIVER:
    friendlyname: "Clay river"
    biome: RIVER
    icon: CLAY
    priority: 10
    contents: SAND:1
    # 50% water required
    watercoverage: 50
    # Conversions - in this case, an adjacent block is required to convert
    # Format is:
    # Original Block% chance:New Block:Adjacent Block
    # So, for below, dirt has a 50% chance of changing into clay if it is next to water!
    # Water in this situation works when flowing (Like how concrete powder turns to concrete with flowing water)
    conversions: DIRT:50:CLAY:WATER
  SAVANNA:
    biome: SAVANNA
    icon: OAK_LEAVES
    priority: 11
    contents: ACACIA_LOG:3 ACACIA_LEAVES:4 GRASS_BLOCK:4
    plants:
      TALL_GRASS: 10:GRASS_BLOCK
  SWAMPLAND:
    friendlyname: "&2Slimy Swamp"
    biome: SWAMP
    icon: LILY_PAD
    priority: 13
    contents: GRASS_BLOCK:4 OAK_LOG:3 OAK_LEAVES:4
    # 50% water coverage required
    watercoverage: 50
    plants:
      # water for plant spawning only counts for water sources
      RED_MUSHROOM: 20:GRASS_BLOCK
      BROWN_MUSHROOM: 20:GRASS_BLOCK
      LILY_PAD: 5:WATER
    mobs:
      SLIME: 5:WATER
    moblimit: 3
